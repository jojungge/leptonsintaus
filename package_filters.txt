#
# Package filtering rules for the LeptonsInTaus project:
#
#########################################################################
# athena 21.2 branch
#########################################################################
+ MuonPerformanceAnalysis/MuonPerformanceCore.*
+ MuonPerformanceAnalysis/athena_21.2/Trigger/TrigAnalysis/TrigMuonMatching
+ MuonPerformanceAnalysis/MuonTPInterfaces.*
+ MuonPerformanceAnalysis/MuonTPTools.*
+ MuonPerformanceAnalysis/ClusterSubmission.*
+ MuonPerformanceAnalysis/MuonPerformanceAlgs.*
#+ MuonPerformanceAnalysis/MuonPtCalibNtupleMaker.*
- MuonPerformanceAnalysis.*
+ IFFTruthClassifier.*
