#include "GaudiKernel/DeclareFactoryEntries.h"
#include "TreeTools/DiMuonTTVATreeTool.h"

DECLARE_NAMESPACE_TOOL_FACTORY(LeptonsInTaus, DiMuonTTVATreeTool)
DECLARE_FACTORY_ENTRIES(TreeTools) { DECLARE_NAMESPACE_TOOL(LeptonsInTaus, DiMuonTTVATreeTool) }
