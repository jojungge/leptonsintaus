#ifndef LEPTONSINTAUS_TRACKINGBRANCHES_H
#define LEPTONSINTAUS_TRACKINGBRANCHES_H
#include <TreeTools/Defs.h>
#include <TreeTools/LeptonsInTausTree.h>

namespace LeptonsInTaus {

    class VertexBranch : public IMuonAnalysisBranch {
    public:
        VertexBranch(const std::string& vtx_name, LeptonsInTauTree& tree, bool is_common = false);
        ~VertexBranch();

        /// Call the fill function
        bool fill() override;
        /// Connect the branch with the TTree object
        bool init() override;
        /// Name of the branch/ branch collection
        std::string name() const override;
        /// The underlying tree pointer to which the branch is connected
        TTree* tree() const override;
        /// Has the initialize function been called on the object
        bool initialized() const override;

        void set_vertex(const xAOD::Vertex* vtx);

    private:
        std::string m_vtx_collection;
        LeptonsInTauTree& m_parent;
        /// Number of associated tracks to the corresponding vertex
        ScalarBranch<unsigned int> m_num_assoc_trks;
        ScalarBranch<unsigned int> m_num_assoc_trks_1000;
        ScalarBranch<unsigned int> m_num_assoc_trks_1500;
        ScalarBranch<unsigned int> m_num_assoc_trks_5000;
        ScalarBranch<float> m_assoc_vtx_pt2;

        const xAOD::Vertex* m_vtx;
        bool m_vtx_set;
    };

    /// Branch collection to save basic properties of a track
    class TrackBranch : public IVariableParticleBranch {
    public:
        virtual ~TrackBranch();
        TrackBranch(LeptonsInTauTree& tree, const std::string& trk_coll_name);

        /// Call the fill function
        bool fill() override;
        /// Connect the branch with the TTree object
        bool init() override;
        /// Name of the branch/ branch collection
        std::string name() const override;
        /// The underlying tree pointer to which the branch is connected
        TTree* tree() const override;
        /// Has the initialize function been called on the object
        bool initialized() const override;

        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;

        template <typename T> bool addVariable(const std::string& var, const std::string& acc = "");
        template <typename T> bool addVariable(const T& def_val, const std::string& var, const std::string& acc = "");
        bool addVariable(std::shared_ptr<IVariableParticleBranch> br);
        template <typename T> std::shared_ptr<ParticleVariableBranch<T>> getBranch(const std::string& var) const;

        void disable_vertexing();

    private:
        LeptonsInTauTree& m_parent;
        std::string m_trk_name;
        /// track pt
        ScalarBranch<float> m_pt;
        /// Number of associated tracks to the corresponding vertex
        std::unique_ptr<VertexBranch> m_assoc_vtx;

        std::vector<std::shared_ptr<IVariableParticleBranch>> m_extra_branches;
    };
    /// Save the covariance of the associated Track
    class CovarianceBranch : public ArrayBranch<float>, virtual public IVariableParticleBranch {
    public:
        CovarianceBranch(TTree* t, const std::string& _branch_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;
    };
    /// Save the charge of the muon or the tau
    class ChargeBranch : public ScalarBranch<short int>, virtual public IVariableParticleBranch {
    public:
        ChargeBranch(TTree* t, const std::string& part_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;
    };
    /// Save the d0 significance
    class D0SigBranch : public ScalarBranch<float>, virtual public IVariableParticleBranch {
    public:
        D0SigBranch(LeptonsInTauTree& parent, const std::string& part_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;

    private:
        LeptonsInTauTree& m_parent;
    };
    /// Save the z0 associated with the primary vertex
    class Z0Branch : public ScalarBranch<float>, virtual public IVariableParticleBranch {
    public:
        Z0Branch(LeptonsInTauTree& parent, const std::string& part_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;

    private:
        LeptonsInTauTree& m_parent;
    };
    /// Save the pt error
    class PtErrBranch : public ScalarBranch<float>, virtual public IVariableParticleBranch {
    public:
        PtErrBranch(TTree* t, const std::string& part_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;
    };
    /// Save the tau prongness
    class ProngnessBranch : public ScalarBranch<unsigned short>, virtual public IVariableParticleBranch {
    public:
        ProngnessBranch(TTree* t, const std::string& part_name);
        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;
    };
}  // namespace LeptonsInTaus
#include <TreeTools/TrackingBranches.ixx>
#endif
