#ifndef LEPTONSINTAUS_PARTICLECLASSBRANCHES_H
#define LEPTONSINTAUS_PARTICLECLASSBRANCHES_H
#include <AsgTools/ToolHandle.h>
#include <MuonTPInterfaces/IMuonTPTrigUtils.h>
#include <TreeTools/Defs.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TreeTools/TrackingBranches.h>

class IIFFTruthClassifier;
namespace Trig {
    class IMatchingTool;
}

namespace LeptonsInTaus {
    class IFFClassBranch : public ParticleVariableBranch<unsigned short> {
    public:
        IFFClassBranch(TTree* t, const std::string& coll_name, const ToolHandle<IIFFTruthClassifier>& classifier);
        NO_COPY_CTOR(IFFClassBranch)
        using ParticleVariableBranch<unsigned short>::setParticle;
        using ParticleVariableBranch<unsigned short>::operator=;

        void setParticle(const xAOD::IParticle* p) override;

    private:
        ToolHandle<IIFFTruthClassifier> m_truth_classifier;
    };

    class TauMarkerBranch : public ParticleVariableBranch<bool> {
    public:
        TauMarkerBranch(TTree* t, const std::string& var_name);
        using ParticleVariableBranch<bool>::setParticle;
        using ParticleVariableBranch<bool>::operator=;

        void setParticle(const xAOD::IParticle* p) override;
    };

    class MCTruthClassifierBranch : public IVariableParticleBranch {
    public:
        MCTruthClassifierBranch(TTree* t, const std::string& coll_name);
        bool fill() override;
        bool init() override;
        std::string name() const override;
        TTree* tree() const override;
        bool initialized() const override;

        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;

    private:
        std::string m_trk_coll;
        ScalarBranch<int> m_truth_type;
        ScalarBranch<int> m_truth_origin;
    };

    class TrigMatchBranch : public ParticleVariableBranch<bool> {
    public:
        TrigMatchBranch(MuonAnalysisTree& parent, const std::string& part_name,

                        const std::string& trig_item, const ToolHandle<IMuonTPTrigUtils>& trigTool);
        using ParticleVariableBranch<bool>::setParticle;
        using ParticleVariableBranch<bool>::operator=;

        void setParticle(const xAOD::IParticle* p) override;

    private:
        ToolHandle<IMuonTPTrigUtils> m_trig_utils;
        std::string m_item;
        std::shared_ptr<ScalarBranch<bool>> m_trig_dec_branch;
    };

}  // namespace LeptonsInTaus
#endif
