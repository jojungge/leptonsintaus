#ifndef LEPTONSINTAUS_TRACKBRANCH_IXX
#define LEPTONSINTAUS_TRACKBRANCH_IXX

#include <TreeTools/TrackingBranches.h>

#include <algorithm>
#include <functional>

namespace LeptonsInTaus {
    //########################################################################################
    //                      TrackBranch
    //########################################################################################
    template <typename T> bool TrackBranch::addVariable(const std::string& var, const std::string& acc) {
        if (m_pt.initialized()) {
            Error("TrackBranch()", "Could not add the variable %s", var.c_str());
            return false;
        }
        if (getBranch<T>(var)) {
            Warning("TrackBranch()", "The variable %s has already been added", var.c_str());
            return true;
        }
        return addVariable(std::make_shared<ParticleVariableBranch<T>>(tree(), name() + "_" + var, acc.empty() ? var : acc));
    }
    template <typename T> bool TrackBranch::addVariable(const T& def_val, const std::string& var, const std::string& acc) {
        if (!addVariable<T>(var, acc)) return false;
        std::shared_ptr<ParticleVariableBranch<T>> br = getBranch<T>(var);
        br->setDefault(def_val);
        br->disable_failing();
        return true;
    }
    template <typename T> std::shared_ptr<ParticleVariableBranch<T>> TrackBranch::getBranch(const std::string& var) const {
        for (auto& b : m_extra_branches) {
            if (b->name() == name() + "_" + var) return std::dynamic_pointer_cast<ParticleVariableBranch<T>>(b);
        }
        return std::shared_ptr<ParticleVariableBranch<T>>();
    }
}  // namespace LeptonsInTaus
#endif
