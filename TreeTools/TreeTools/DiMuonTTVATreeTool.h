#ifndef LEPTONSINTAUS_DIMUONTTVATREETOOL_H
#define LEPTONSINTAUS_DIMUONTTVATREETOOL_H

#include <AsgTools/AsgTool.h>
#include <MuonTPTools/DiMuonTPTreeTool.h>
#include <TreeTools/ITPMuonTTVATreeTool.h>
#include <TreeTools/TrackingBranches.h>

namespace CP {
    class IMuonSelectionTool;
}
class IIFFTruthClassifier;
class IMuonJetCalibTool;

namespace LeptonsInTaus {

    class DiMuonTTVATreeTool : public asg::AsgTool, public ITPMuonTTVATreeTool {
    public:
        DiMuonTTVATreeTool(const std::string& name);
        virtual ~DiMuonTTVATreeTool();
        // Athena Constructor
        ASG_TOOL_CLASS(DiMuonTTVATreeTool, ITPMuonTTVATreeTool)

        StatusCode fill(const xAOD::IParticle* tag, const xAOD::Muon* probe) override;

        StatusCode setup_tree(std::shared_ptr<LeptonsInTauTree> tree_ptr) override;

    private:
        template <typename T> std::shared_ptr<ScalarBranch<T>> make_branch(const std::string& br_name);

        bool m_is_taumu_tp;
        /// Runs on Monte Carlo
        bool m_is_mc;
        std::string m_met_container;
        std::string m_met_term;
        /// IFF classification
        ToolHandle<IIFFTruthClassifier> m_iff_truth_classifier;
        ToolHandle<CP::IMuonSelectionTool> m_muo_sel_tool;
        ToolHandle<IMuonJetCalibTool> m_jet_calib_tool;

        std::shared_ptr<LeptonsInTauTree> m_tree;

        std::shared_ptr<IParticleFourMomBranch> m_tag;
        /// Probe tracks
        std::shared_ptr<IParticleFourMomBranch> m_probe;
        std::shared_ptr<TrackBranch> m_ID_tracks;
        std::shared_ptr<TrackBranch> m_ME_tracks;

        typedef ScalarBranch<float> FloatBranch;
        typedef ScalarBranch<bool> BoolBranch;
        typedef ScalarBranch<unsigned short> UshortBranch;
        /// Invariant four momentum
        std::shared_ptr<FloatBranch> m_dilep_mll;
        std::shared_ptr<FloatBranch> m_dilep_pt;
        std::shared_ptr<FloatBranch> m_dilep_eta;
        std::shared_ptr<FloatBranch> m_dilep_phi;
        /// Delta phi of the consitutent particles
        std::shared_ptr<FloatBranch> m_dilep_dphi;
        std::shared_ptr<FloatBranch> m_dilep_deta;
        std::shared_ptr<FloatBranch> m_sum_cos_dphi;

        std::shared_ptr<FloatBranch> m_probe_mt;
        std::shared_ptr<BoolBranch> m_probe_pass_lowpt;
        std::shared_ptr<BoolBranch> m_probe_pass_highpt;
        std::shared_ptr<UshortBranch> m_probe_quality;
    };
    template <typename T> std::shared_ptr<ScalarBranch<T>> DiMuonTTVATreeTool::make_branch(const std::string& br_name) {
        if (!m_tree) { throw std::runtime_error("No tree has been assigned yet"); }
        std::shared_ptr<ScalarBranch<T>> br = std::make_shared<ScalarBranch<T>>(m_tree->tree(), br_name);
        m_tree->addBranch(br);
        return m_tree->get_branch<ScalarBranch<T>>(br->name());
    }
}  // namespace LeptonsInTaus
#endif
