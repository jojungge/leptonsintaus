#ifndef LEPTONSINTAUS_DEFS_H
#define LEPTONSINTAUS_DEFS_H

#include <xAODEgamma/Electron.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/Muon.h>
#include <xAODTracking/TrackParticle.h>
#include <xAODTracking/Vertex.h>
namespace LeptonsInTaus {
    class TrackBranch;

    void configure_id_tracks(std::shared_ptr<LeptonsInTaus::TrackBranch> id_branch);

    bool isInContainer(const xAOD::IParticleContainer* container, const xAOD::IParticle* to_test);

    float CalcMt(const xAOD::IParticle* p1, const xAOD::IParticle* p2);
    float CalcMt(const xAOD::IParticle* p1, const xAOD::MissingET* met);

}  // namespace LeptonsInTaus
#endif
