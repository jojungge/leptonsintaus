#ifndef LEPTONSINTAUS_ITPMuonTTVATreeTool_H
#define LEPTONSINTAUS_ITPMuonTTVATreeTool_H

#include <AsgTools/IAsgTool.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <xAODBase/IParticle.h>
#include <xAODMuon/Muon.h>
#include <xAODTau/TauJet.h>

#include <memory>
namespace LeptonsInTaus {
    class ITPMuonTTVATreeTool : virtual public asg::IAsgTool {
    public:
        ASG_TOOL_INTERFACE(ITPMuonTTVATreeTool)
        virtual ~ITPMuonTTVATreeTool() = default;

        /// Fill the T&P pair
        virtual StatusCode fill(const xAOD::IParticle* tag, const xAOD::Muon* probe) = 0;

        virtual StatusCode setup_tree(std::shared_ptr<LeptonsInTauTree> tree_ptr) = 0;
    };
}  // namespace LeptonsInTaus
#endif
