#ifndef LEPTONSINTAUS_LEPTONSINTAUSTREE_H
#define LEPTONSINTAUS_LEPTONSINTAUSTREE_H
#include <MuonPerformanceCore/MuonAnalysisTree.h>
#include <TreeTools/Defs.h>

namespace LeptonsInTaus {

    class LeptonsInTauTree : public MuonAnalysisTree {
    public:
        LeptonsInTauTree(const std::string& t_name, const std::string& path = "");
        LeptonsInTauTree(const LeptonsInTauTree&) = delete;
        void operator=(const LeptonsInTauTree&) = delete;
        virtual ~LeptonsInTauTree();

        const xAOD::EventInfo* get_info();
        const xAOD::Vertex* get_prim_vtx();

        void release_common_containers();

    private:
        void load_common_containers();
        const xAOD::EventInfo* m_ev_info;
        const xAOD::Vertex* m_prim_vtx;
    };

}  // namespace LeptonsInTaus
#endif
