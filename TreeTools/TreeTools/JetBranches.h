#ifndef LEPTONSINTAUS_JETBRANCHES_H
#define LEPTONSINTAUS_JETBRANCHES_H
#include <MuonTPTools/JetBranch.h>
#include <TreeTools/Defs.h>
#include <TreeTools/LeptonsInTausTree.h>
namespace LeptonsInTaus {
    class ClosestJetBranch : public IVariableParticleBranch {
    public:
        bool init() override;
        bool fill() override;
        std::string name() const override;
        bool initialized() const override;

        TTree* tree() const override;

        void setParticle(const xAOD::IParticle* p) override;
        void setParticle(const xAOD::IParticle& p) override;
        void operator=(const xAOD::IParticle* p) override;
        void operator=(const xAOD::IParticle& p) override;

        virtual ~ClosestJetBranch();
        ClosestJetBranch(TTree* tree, const std::string& jets, const ToolHandle<IMuonJetCalibTool>& jet_calib_tool);

    protected:
        std::string m_jet_collection;
        ToolHandle<IMuonJetCalibTool> m_calib_tool;
        IParticleFourMomBranch m_close_jet;
        ScalarBranch<float> m_dR;
    };

}  // namespace LeptonsInTaus

#endif
