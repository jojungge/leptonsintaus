#! /usr/bin/env python
from ClusterSubmission.Utils import CheckPandaSetup, ReadListFromFile, prettyPrint, ResolvePath, FillWhiteSpaces
from ClusterSubmission.RucioListBuilder import GetScopes
from ClusterSubmission.ClusterEngine import ATLASPROJECT, ATLASVERSION, TESTAREA
from ClusterSubmission.ListDisk import RUCIO_ACCOUNT
import os, argparse, commands, sys, logging

RSE = os.getenv("RUCIO_PERFRSE") if os.getenv("RUCIO_PERFRSE") else "-1"

if ATLASPROJECT == None:
    logging.error("Please setup AthDerivation")
    exit(1)

if RUCIO_ACCOUNT == None:
    logging.error("No RUCIO ACCOUNT is available.. please define a rucio Account in RUCIO_ACCOUNT")
    exit(1)


def setupGridSubmitArgParser():
    parser = argparse.ArgumentParser(
        description=
        'This script submits the analysis code to the grid. For more help type \"python MuonPerformanceAlgs/scripts/SubmitToGrid.py -h\"',
        prog='SubmitToGrid',
        conflict_handler='resolve',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--Test', help='run in test modus, i.e. without DS replication', action='store_true', default=False)
    parser.add_argument('--destSE',
                        help='specify a destination for replication apart from to %s (or \"-1\" for no replication)' % RSE,
                        default='%s' % RSE)
    parser.add_argument('--DuplicateTask',
                        help='You can create another job with the same output dataset...',
                        action='store_true',
                        default=False)
    parser.add_argument(
        '--FilesPerJob',
        help=
        'You can specify the number of files per job... If you have data then it would be may be better to give a larger number... Default: 5',
        type=int,
        default=0)
    parser.add_argument('--nJobs', help='Specifiy the maximum number of jobs you want to run ', type=int, default=0)
    parser.add_argument('--ProdNumber', help='Which is the produciton number to be submitted', required=True)
    parser.add_argument('-i', '--inputDS', help='input dataset or DS list', nargs="+", default=[])
    parser.add_argument('--noAmiCheck',
                        help='disables the check of the existence of the input datasets.',
                        action="store_false",
                        default=True)
    parser.add_argument('--private', help='Submits the production without MCP group production rights', action="store_true", default=False)
    parser.add_argument('--site', help='Which site should be used to process T&P', default="AUTO")
    parser.add_argument("--groupRights", help="Which group rights should be used", default="perf-muons")
    parser.add_argument("--jobOptions",
                        help="Job options to use for the job",
                        required=True,
                        choices=[
                            "SelectionAlgs/runTPAnalysis.py",
                            "SelectionAlgs/runTTVATreeMaker.py",
                            "MuonPerformanceAlgs/MuonTPTriggerFinder.py",
                        ])
    return parser


def GetTarBallOpt():
    TarList = []
    if not os.path.isfile(TESTAREA + "/TarBall.tgz"):
        TarList = [
            "--extFile=*.root",
            "--outTarBall=%s/TarBall.tgz" % (TESTAREA),
            "--excludeFile='*.svn*','*.git*','*.pyc','*.*~','*.tex','*.tmp','*.pdf','*.png','*.log','*.dat','*.core','*README*'"
        ]
    else:
        TarList = ["--inTarBall=%s/TarBall.tgz" % (TESTAREA)]
    return TarList


def PrepareInputFiles(InputConfigs=[], AmiCheck=True):
    DSList = []
    for In in InputConfigs:
        if not ResolvePath(In): DSList.append(In)
        else: DSList.extend(ReadListFromFile(In))
    if not AmiCheck: return DSList
    CheckedDS = [DS for DS in DSList if GetNFiles(DS) > 0]
    return CheckedDS


def GetoutDSName(DS, prod_name="LeptonsInTau"):
    outDS = DS.replace("/", "").strip()
    if outDS.find(":") > -1:
        outDS = outDS[DS.find(":") + 1:]
    scope = outDS.split(".")[0]
    print outDS
    if "data" not in scope:
        outDS = ".".join(outDS.split(".")[0:3]) + ".%s." % (prod_name) + outDS[outDS.rfind("_r") + 1:]
    else:
        outDS = ".".join(outDS.split(".")[0:2] + [prod_name, outDS.split(".")[-1]])
    return outDS


def GetNFiles(ds):
    Cmd = "rucio list-files %s --csv" % (ds)
    N_Files = commands.getoutput(Cmd)
    if len(N_Files) == 0: return 0
    return len(N_Files.split("\n"))


def SubmitJob(DataSet, RunOptions):
    os.chdir(TESTAREA)

    Pathena_Options = []
    Pathena_Options.extend(GetTarBallOpt())
    Pathena_Options.append("--inDS=\"%s\"" % (DataSet))
    Pathena_Options.append("--express")
    Pathena_Options.append("--mergeOutput")
    Pathena_Options.append("--useNewCode")
    Pathena_Options.append("--destSE='MPPMU_PERF-MUONS'")

    #Pathena_Options.append("--matchingOS")

    FilesPerJob = RunOptions.FilesPerJob
    nJobs = RunOptions.nJobs

    # Additional Options parsing to the pathena Command
    if RunOptions.Test:
        FilesPerJob = 1
        nJobs = 1
        Pathena_Options.append("--nFiles=1")
        Pathena_Options.append("--disableAutoRetry")
    if not RunOptions.Test and RunOptions.destSE != '-1': Pathena_Options.append("--destSE=%s" % RunOptions.destSE)
    if RunOptions.Test or RunOptions.DuplicateTask: Pathena_Options.append("--allowTaskDuplication")
    if nJobs > 0: Pathena_Options.append("--nJobs=%d " % nJobs)
    if FilesPerJob > 0: Pathena_Options.append("--FilesPerJob=%d" % (FilesPerJob))
    # Sets the OutputDataSetName
    OutDS = ""
    stream = "LeptonsInTau"
    if RunOptions.jobOptions == "SelectionAlgs/runTPAnalysis.py":
        stream = "ZtaumuTP"
    if RunOptions.jobOptions == "MuonPerformanceAlgs/MuonTPTriggerFinder.py":
        stream = "UpsilonTrig"
    if not RunOptions.private:
        OutDS = "group.%s.%s_%s" % (RunOptions.groupRights, GetoutDSName(DataSet, stream), RunOptions.ProdNumber.lower())
        Pathena_Options.append("--official")
        Pathena_Options.append("--voms=atlas:/atlas/%s/Role=production" % (RunOptions.groupRights))
    else:
        OutDS = "user.%s.%s_%s" % (RUCIO_ACCOUNT, GetoutDSName(DataSet, stream), RunOptions.ProdNumber.lower())
    Pathena_Options.append("--outDS=\"%s\"" % OutDS)
    Pathena_Options.append("--site=\"%s\"" % RunOptions.site)

    print(
        "################################################################################################################################################################"
    )
    print("                                                                        MCP Tag & Probe on the grid")
    print(
        "################################################################################################################################################################"
    )
    # prettyPrint('USER', USERNAME)
    prettyPrint('RUCIO', RUCIO_ACCOUNT)
    prettyPrint("ATLASPROJECT", ATLASPROJECT)
    prettyPrint("ATLASVERSION", ATLASVERSION)
    prettyPrint("TESTAREA", TESTAREA)
    print(
        "################################################################################################################################################################"
    )
    print("                                                                        JobOptions")
    print(
        "################################################################################################################################################################"
    )
    prettyPrint('InputDataSet:', DataSet)
    prettyPrint('OutputContainer', OutDS)
    prettyPrint('FilesPerJob', str(FilesPerJob))
    if nJobs > 0: prettyPrint('NumberOfJobs', str(nJobs))

    if len(GetTarBallOpt()) == 1:
        prettyPrint('', 'Using already existing tarball located at:', width=32, separator='->')
        prettyPrint('', '%s/TarBall.tgz' % TESTAREA, width=34, separator='')

    prettyPrint("Pathena Options:", "")
    for Opt in Pathena_Options:
        prettyPrint("", Opt, separator="***")
    print(
        "################################################################################################################################################################"
    )
    ExeCmd = RunOptions.jobOptions

    Command = "pathena %s %s" % (ExeCmd, " ".join(Pathena_Options))

    logging.info('\nSubmitting using command:\n%s\n...' % Command)
    os.system(Command)


if __name__ == '__main__':
    RunOptions = setupGridSubmitArgParser().parse_args()
    CheckPandaSetup()

    DataSets = PrepareInputFiles(RunOptions.inputDS, RunOptions.noAmiCheck)
    if len(DataSets) == 0:
        logging.error("No valid datasets were found for Tag & Probe")
        exit(1)
    for DS in DataSets:
        SubmitJob(DS, RunOptions)
