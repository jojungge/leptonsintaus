#include <MuonTPTools/Utils.h>
#include <TreeTools/Defs.h>
#include <TreeTools/JetBranches.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <xAODMissingET/MissingETContainer.h>
namespace LeptonsInTaus {

    bool ClosestJetBranch::init() { return m_close_jet.init() && m_dR.init(); }
    bool ClosestJetBranch::fill() { return m_close_jet.fill() && m_dR.fill(); }
    std::string ClosestJetBranch::name() const { return m_jet_collection; }
    bool ClosestJetBranch::initialized() const { return m_close_jet.initialized(); }

    TTree* ClosestJetBranch::tree() const { return m_close_jet.tree(); }

    void ClosestJetBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::Jet* close_jet = m_calib_tool->GetClosestJet(p);
        if (close_jet) {
            m_close_jet = close_jet;
            m_dR = DeltaR(close_jet, p);
        }
    }
    void ClosestJetBranch::setParticle(const xAOD::IParticle& p) { setParticle(&p); }
    void ClosestJetBranch::operator=(const xAOD::IParticle* p) { setParticle(p); }
    void ClosestJetBranch::operator=(const xAOD::IParticle& p) { setParticle(p); }
    ClosestJetBranch::~ClosestJetBranch() = default;
    ClosestJetBranch::ClosestJetBranch(TTree* tree, const std::string& jets, const ToolHandle<IMuonJetCalibTool>& jet_calib_tool) :
        m_jet_collection(jets + "_jet"), m_calib_tool(jet_calib_tool), m_close_jet(tree, name()), m_dR(tree, name() + "_dR", -1) {
        m_close_jet.disable_failing();
        m_close_jet.addVariable<float>(0., "jvt", "Jvt");
        m_close_jet.addVariable<float>(0., "emfrac", "EMFrac");
        m_close_jet.addVariable<char>(0, "passOR");
        for (auto& WP : m_calib_tool->GetBTagWPs()) {
            m_close_jet.addVariable<bool>(false, "isB_" + WP, "isBtag_" + WP);
            // if (!m_parent.isData()) {
            //    m_close_jet.addVariable<float>(1., "btagSF_" + WP, "BtagSF_" + WP);
            //    m_parent.registerBranch(std::make_shared<MuonTPBTagSFBranch>(m_parent.common_tree(), m_calib_tool, name(), WP));
            //}
        }
    }

}  // namespace LeptonsInTaus
