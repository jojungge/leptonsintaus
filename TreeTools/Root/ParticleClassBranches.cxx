#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonPerformanceCore/MuonTriggerBranches.h>
#include <MuonTPTools/Utils.h>
#include <TreeTools/ParticleClassBranches.h>
namespace LeptonsInTaus {
    //########################################################
    //                   IFFClassBranch
    //########################################################
    IFFClassBranch::IFFClassBranch(TTree* t, const std::string& var_name, const ToolHandle<IIFFTruthClassifier>& classifier) :
        ParticleVariableBranch<unsigned short>(t, var_name + "_IFF_type"), m_truth_classifier(classifier) {}
    void IFFClassBranch::setParticle(const xAOD::IParticle* p) {
        static const IntDecorator dec_truth_type("truthType");
        static const IntDecorator dec_truth_origin("truthOrigin");
        static const IntAccessor acc_truth_type("truthType");
        static const IntAccessor acc_truth_origin("truthOrigin");

        if (!acc_truth_type.isAvailable(*p) || acc_truth_type(*p) <= 0) { dec_truth_type(*p) = getParticleTruthType(p); }
        if (!acc_truth_origin.isAvailable(*p) || acc_truth_origin(*p) <= 0) { dec_truth_origin(*p) = getParticleTruthOrigin(p); }
        ScalarBranch<unsigned short>::operator=(static_cast<int>(m_truth_classifier->classify(*p)));
    }
    //########################################################
    //                   TauMarkerBranch
    //########################################################
    TauMarkerBranch::TauMarkerBranch(TTree* t, const std::string& var_name) : ParticleVariableBranch<bool>(t, var_name) {}
    void TauMarkerBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TruthParticle* truth_p = getTruthMatchedParticle(p);
        if (truth_p) {
            truth_p = GetFirstChainLink(truth_p);
            for (size_t p = 0; p < truth_p->nParents(); ++p) {
                const xAOD::TruthParticle* parent = truth_p->parent(p);
                if (parent && parent->isTau()) {
                    ScalarBranch<bool>::operator=(true);
                    return;
                }
            }
        }
        ScalarBranch<bool>::operator=(false);
    }

    //########################################################
    //                   MuonTrigMatchBranch
    //########################################################
    TrigMatchBranch::TrigMatchBranch(MuonAnalysisTree& parent, const std::string& part_name, const std::string& trig_item,
                                     const ToolHandle<IMuonTPTrigUtils>& trigTool) :
        ParticleVariableBranch<bool>(parent.tree(), part_name + "_matched_" + trig_item),
        m_trig_utils(trigTool),
        m_item(trig_item),
        m_trig_dec_branch(nullptr) {
        std::shared_ptr<TriggerDecisionBranch> trig_branch =
            std::make_shared<TriggerDecisionBranch>(parent.common_tree(), trigTool, trig_item);
        parent.addBranch(trig_branch);
        m_trig_dec_branch = parent.get_branch<ScalarBranch<bool>>(trig_branch->name());
        setDefault(false);
        disable_failing();
    }
    void TrigMatchBranch::setParticle(const xAOD::IParticle* p) {
        if (!m_trig_dec_branch->fill() || !m_trig_dec_branch->getVariable()) return;
        const xAOD::Muon* muon = dynamic_cast<const xAOD::Muon*>(p);
        if (!muon) return;
        setValue(m_trig_utils->Trig_Match(*muon, m_item));
    }
    //#######################################################
    //              MCTruthClassifierBranch
    //#######################################################
    MCTruthClassifierBranch::MCTruthClassifierBranch(TTree* t, const std::string& coll_name) :
        m_trk_coll(coll_name), m_truth_type(t, coll_name + "_truthType", -1), m_truth_origin(t, coll_name + "_truthOrigin", -1) {}
    bool MCTruthClassifierBranch::fill() { return m_truth_type.fill() && m_truth_origin.fill(); }
    bool MCTruthClassifierBranch::init() { return m_truth_origin.init() && m_truth_type.init(); }
    std::string MCTruthClassifierBranch::name() const { return m_trk_coll + "_truth "; }
    TTree* MCTruthClassifierBranch::tree() const { return m_truth_origin.tree(); }
    bool MCTruthClassifierBranch::initialized() const { return m_truth_origin.initialized(); }
    SET_PARTICLE(MCTruthClassifierBranch)
    void MCTruthClassifierBranch::setParticle(const xAOD::IParticle* p) {
        m_truth_type = getParticleTruthType(p);
        m_truth_origin = getParticleTruthOrigin(p);
    }
}  // namespace LeptonsInTaus
