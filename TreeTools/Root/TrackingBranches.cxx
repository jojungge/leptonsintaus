#include <MuonTPTools/Utils.h>
#include <TreeTools/Defs.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TreeTools/TrackingBranches.h>
#include <xAODMuon/Muon.h>
#include <xAODTau/TauJet.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>

namespace {
    const xAOD::TrackParticle* to_track(const xAOD::IParticle* p) {
        if (!p)
            return nullptr;
        else if (p->type() == xAOD::Type::Muon) {
            return dynamic_cast<const xAOD::Muon*>(p)->primaryTrackParticle();
        } else if (p->type() == xAOD::Type::TrackParticle) {
            return dynamic_cast<const xAOD::TrackParticle*>(p);
        } else if (p->type() == xAOD::Type::Tau) {
            const xAOD::TauJet* tau = dynamic_cast<const xAOD::TauJet*>(p);
            std::vector<const xAOD::TrackParticle*> tau_tracks;
            for (const auto& itrack : tau->tracks()) {
                const xAOD::TrackParticle* track = itrack->track();
                if (track) tau_tracks.push_back(track);
            }
            std::sort(tau_tracks.begin(), tau_tracks.end(), [tau](const xAOD::TrackParticle* trk1, const xAOD::TrackParticle* trk2) {
                if (trk1->charge() != trk2->charge()) return tau->charge() == trk1->charge();
                return trk1->pt() > trk2->pt();
            });
            if (!tau_tracks.empty()) return tau_tracks[0];
        }
        return nullptr;
    }
}  // namespace

namespace LeptonsInTaus {
    TrackBranch::~TrackBranch() {
        m_parent.removeBranch(m_pt);
        for (auto& br : m_extra_branches) { m_parent.removeBranch(*br); }
    }
    void TrackBranch::disable_vertexing() {
        if (!m_pt.initialized()) m_assoc_vtx.reset();
    }
    TrackBranch::TrackBranch(LeptonsInTauTree& tree, const std::string& trk_coll_name) :
        m_parent(tree),
        m_trk_name(trk_coll_name),
        m_pt(tree.tree(), trk_coll_name + "_pt"),
        m_assoc_vtx(std::make_unique<VertexBranch>(trk_coll_name + "_assocVtx", tree)),
        m_extra_branches() {
        addVariable<float>("phi");
        addVariable<float>("qOverP");
        addVariable<float>("theta");
        addVariable<float>("d0");
        addVariable<float>("vz");
        addVariable<float>("z0_assoc", "z0");
        addVariable<float>("chi2", "chiSquared");
        addVariable<float>("nDoF", "numberDoF");

        addVariable(std::make_shared<CovarianceBranch>(tree.tree(), trk_coll_name));
        addVariable(std::make_shared<D0SigBranch>(tree, trk_coll_name));
        addVariable(std::make_shared<Z0Branch>(tree, trk_coll_name));
        addVariable(std::make_shared<PtErrBranch>(tree.tree(), trk_coll_name));

        m_parent.addBranch(m_pt);
    }
    bool TrackBranch::addVariable(std::shared_ptr<IVariableParticleBranch> br) {
        if (!br) return false;
        m_extra_branches.push_back(br);
        return m_parent.addBranch(m_extra_branches.back().get());
    }
    bool TrackBranch::fill() {
        for (auto br : m_extra_branches) {
            if (!br->fill()) return false;
        }
        if (!m_pt.fill() || (m_assoc_vtx && !m_assoc_vtx->fill())) { return false; }
        return true;
    }
    bool TrackBranch::init() { return true; }
    std::string TrackBranch::name() const { return m_trk_name; }
    /// The underlying tree pointer to which the branch is connected
    TTree* TrackBranch::tree() const { return m_parent.tree(); }
    /// Has the initialize function been called on the object
    bool TrackBranch::initialized() const { return true; }

    SET_PARTICLE(TrackBranch)
    void TrackBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TrackParticle* trk = nullptr;
        if (p && p->type() == xAOD::Type::TrackParticle) { trk = dynamic_cast<const xAOD::TrackParticle*>(p); }
        if (!trk) return;
        // Set the extra branches
        for (auto& br : m_extra_branches) { br->setParticle(trk); }

        m_pt = trk->pt();
        if (m_assoc_vtx) { m_assoc_vtx->set_vertex(trk->vertex()); }
    }
    ///###################################################################
    ///              CovarianceBranch
    ///###################################################################
    CovarianceBranch::CovarianceBranch(TTree* t, const std::string& trk_coll) : ArrayBranch<float>(t, trk_coll + "_cov", 15, FLT_MAX) {}
    SET_PARTICLE(CovarianceBranch)
    void CovarianceBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TrackParticle* trk = to_track(p);
        if (trk && xAOD::TrackingHelpers::hasValidCov(trk)) {
            const std::vector<float> cov_vec = trk->definingParametersCovMatrixVec();
            for (size_t i = 0; i < cov_vec.size(); ++i) { set(i, cov_vec[i]); }
        }
    }
    ///###################################################################
    ///              ChargeBranch
    ///###################################################################
    ChargeBranch::ChargeBranch(TTree* t, const std::string& part_name) : ScalarBranch<short int>(t, part_name + "_q") {}
    SET_PARTICLE(ChargeBranch)
    void ChargeBranch::setParticle(const xAOD::IParticle* p) {
        if (!p) return;
        if (p->type() == xAOD::Type::Muon) {
            setValue(dynamic_cast<const xAOD::Muon*>(p)->charge());
        } else if (p->type() == xAOD::Type::Tau) {
            setValue(dynamic_cast<const xAOD::TauJet*>(p)->charge());
        }
    }
    ///###################################################################
    ///              Z0Branch
    ///###################################################################
    Z0Branch::Z0Branch(LeptonsInTauTree& parent, const std::string& part_name) :
        ScalarBranch<float>(parent.tree(), part_name + "_z0_primary", 1.e4), m_parent(parent) {}
    SET_PARTICLE(Z0Branch)
    void Z0Branch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TrackParticle* trk = to_track(p);
        if (!trk) { return; }
        /// Distance w.r.t. primary vertex
        setValue(trk->z0() + trk->vz() - (m_parent.get_prim_vtx() ? m_parent.get_prim_vtx()->z() : 0.));
    }
    ///###################################################################
    ///              D0SigBranch
    ///###################################################################
    D0SigBranch::D0SigBranch(LeptonsInTauTree& parent, const std::string& part_name) :
        ScalarBranch<float>(parent.tree(), part_name + "_d0_significance", 1.e3), m_parent(parent) {}
    SET_PARTICLE(D0SigBranch)
    void D0SigBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TrackParticle* trk = to_track(p);
        if (!trk) return;
        float d0sign = 1.e3;
        try {
            d0sign = xAOD::TrackingHelpers::d0significance(trk, m_parent.get_info()->beamPosSigmaX(), m_parent.get_info()->beamPosSigmaY(),
                                                           m_parent.get_info()->beamPosSigmaXY());
        } catch (...) { d0sign = 1.e3; }
        setValue(d0sign);
    }
    ///###################################################################
    ///              PtErrBranch
    ///###################################################################
    PtErrBranch::PtErrBranch(TTree* t, const std::string& part_name) : ScalarBranch<float>(t, part_name + "_pt_err", FLT_MAX) {}
    SET_PARTICLE(PtErrBranch)
    void PtErrBranch::setParticle(const xAOD::IParticle* p) {
        const xAOD::TrackParticle* trk = to_track(p);
        if (!trk) return;
        float pt_err = FLT_MAX;
        try {
            pt_err = xAOD::TrackingHelpers::pTErr(trk);
        } catch (...) { pt_err = FLT_MAX; }
        setValue(pt_err);
    }
    ///###################################################################
    ///              ProngnessBranch
    ///###################################################################
    ProngnessBranch::ProngnessBranch(TTree* t, const std::string& part_name) : ScalarBranch<unsigned short>(t, part_name + "_prongs", 0) {}
    SET_PARTICLE(ProngnessBranch)
    void ProngnessBranch::setParticle(const xAOD::IParticle* p) {
        if (!p) return;
        if (p->type() != xAOD::Type::Tau) {
            PromptParticle(p);
            throw std::runtime_error("Only taus are allowed to be given to " + name());
        }
        setValue(dynamic_cast<const xAOD::TauJet*>(p)->nTracks());
    }
    ///###################################################################
    ///              VertexBranch
    ///###################################################################

    VertexBranch::VertexBranch(const std::string& vtx_name, LeptonsInTauTree& tree, bool is_common) :
        m_vtx_collection(vtx_name),
        m_parent(tree),
        m_num_assoc_trks(is_common ? tree.common_tree() : tree.tree(), vtx_name + "_NTrks"),
        m_num_assoc_trks_1000(is_common ? tree.common_tree() : tree.tree(), vtx_name + "_NTrksPt1000"),
        m_num_assoc_trks_1500(is_common ? tree.common_tree() : tree.tree(), vtx_name + "_NTrksPt1500"),
        m_num_assoc_trks_5000(is_common ? tree.common_tree() : tree.tree(), vtx_name + "_NTrksPt5000"),
        m_assoc_vtx_pt2(is_common ? tree.common_tree() : tree.tree(), vtx_name + "_SumPt2"),
        m_vtx(nullptr),
        m_vtx_set(false) {
        m_parent.addBranch(this);
        m_parent.addBranch(m_num_assoc_trks);
        m_parent.addBranch(m_num_assoc_trks_1000);
        m_parent.addBranch(m_num_assoc_trks_1500);
        m_parent.addBranch(m_num_assoc_trks_5000);
        m_parent.addBranch(m_assoc_vtx_pt2);
    }

    VertexBranch::~VertexBranch() {
        m_parent.removeBranch(this);
        m_parent.removeBranch(m_num_assoc_trks);
        m_parent.removeBranch(m_num_assoc_trks_1000);
        m_parent.removeBranch(m_num_assoc_trks_1500);
        m_parent.removeBranch(m_num_assoc_trks_5000);
        m_parent.removeBranch(m_assoc_vtx_pt2);
    }
    /// Call the fill function
    bool VertexBranch::fill() {
        if (!m_vtx_set) {
            Error("VertexBranch::fill()", "set_vertex() method has not been called");
            return false;
        }
        float vtx_pt2 = 0;
        unsigned int n_trks(0), n_trks_1000(0), n_trks_1500(0), n_trks_5000(0);
        if (m_vtx) {
            for (size_t t = 0; t < m_vtx->nTrackParticles(); ++t) {
                const xAOD::TrackParticle* vtx_trk = m_vtx->trackParticle(t);
                vtx_pt2 += vtx_trk ? vtx_trk->pt() * vtx_trk->pt() : 0;
                n_trks += (vtx_trk != nullptr);
                n_trks_1000 += !vtx_trk || vtx_trk->pt() < 1000 ? 0 : 1;
                n_trks_1500 += !vtx_trk || vtx_trk->pt() < 1500 ? 0 : 1;
                n_trks_5000 += !vtx_trk || vtx_trk->pt() < 5000 ? 0 : 1;
            }
        }
        m_num_assoc_trks = n_trks;
        m_num_assoc_trks_1000 = n_trks_1000;
        m_num_assoc_trks_1500 = n_trks_1500;
        m_num_assoc_trks_5000 = n_trks_5000;
        m_assoc_vtx_pt2 = std::sqrt(vtx_pt2);
        m_vtx_set = false;
        m_vtx = nullptr;
        return true;
    }
    bool VertexBranch::init() { return true; }
    std::string VertexBranch::name() const { return m_vtx_collection; }
    TTree* VertexBranch::tree() const { return m_num_assoc_trks.tree(); }
    bool VertexBranch::initialized() const { return true; }
    void VertexBranch::set_vertex(const xAOD::Vertex* vtx) {
        m_vtx_set = true;
        m_vtx = vtx;
    }

}  // namespace LeptonsInTaus
