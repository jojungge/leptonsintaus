#include <MuonPerformanceCore/MuonAnalysisBranch.h>
#include <TreeTools/Defs.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TreeTools/TrackingBranches.h>
#include <xAODTracking/VertexContainer.h>

namespace LeptonsInTaus {

    LeptonsInTauTree::LeptonsInTauTree(const std::string& t_name, const std::string& path) :
        MuonAnalysisTree(t_name, path), m_ev_info(nullptr), m_prim_vtx(nullptr) {}
    LeptonsInTauTree::~LeptonsInTauTree() {}
    const xAOD::EventInfo* LeptonsInTauTree::get_info() {
        load_common_containers();
        return m_ev_info;
    }
    const xAOD::Vertex* LeptonsInTauTree::get_prim_vtx() {
        load_common_containers();
        return m_prim_vtx;
    }
    void LeptonsInTauTree::load_common_containers() {
        if (m_ev_info) { return; }
        const xAOD::VertexContainer* vtx_container = nullptr;
        if (evtStore()->retrieve(m_ev_info, "EventInfo").isFailure()) return;
        if (evtStore()->retrieve(vtx_container, "PrimaryVertices").isFailure()) return;
        for (const auto& vx : *vtx_container) {
            if (vx->vertexType() == xAOD::VxType::PriVtx) {
                m_prim_vtx = vx;
                break;
            }
        }
    }
    void LeptonsInTauTree::release_common_containers() {
        m_ev_info = nullptr;
        m_prim_vtx = nullptr;
    }
}  // namespace LeptonsInTaus
