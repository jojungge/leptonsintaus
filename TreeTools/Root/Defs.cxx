#include <FourMomUtils/xAODP4Helpers.h>
#include <MuonTPTools/Utils.h>
#include <TreeTools/Defs.h>
#include <TreeTools/TrackingBranches.h>
namespace LeptonsInTaus {
    bool isInContainer(const xAOD::IParticleContainer* container, const xAOD::IParticle* to_test) {
        for (const xAOD::IParticle* in_cont : *container) {
            if (IsSame(in_cont, to_test)) { return true; }
        }
        return false;
    }

    float CalcMt(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
        return std::sqrt(2 * p1->pt() * p2->pt() * (1 - std::cos(xAOD::P4Helpers::deltaPhi(p1, p2))));
    }
    float CalcMt(const xAOD::IParticle* p1, const xAOD::MissingET* met) {
        return std::sqrt(2 * p1->pt() * met->met() * (1 - std::cos(xAOD::P4Helpers::deltaPhi(p1, met))));
    }

    void configure_id_tracks(std::shared_ptr<LeptonsInTaus::TrackBranch> ID_tracks) {
        std::vector<std::string> HitSummary{
            "numberOfContribPixelLayers",
            "expectInnermostPixelLayerHit",
            "numberOfInnermostPixelLayerHits",
            "numberOfInnermostPixelLayerOutliers",
            "numberOfInnermostPixelLayerSharedHits",
            "numberOfInnermostPixelLayerSplitHits",
            "expectNextToInnermostPixelLayerHit",
            "numberOfNextToInnermostPixelLayerHits",
            "numberOfNextToInnermostPixelLayerOutliers",
            "numberOfNextToInnermostPixelLayerSharedHits",
            "numberOfNextToInnermostPixelLayerSplitHits",
            "numberOfDBMHits",
            "numberOfPixelHits",
            "numberOfPixelOutliers",
            "numberOfPixelHoles",
            "numberOfPixelSharedHits",
            "numberOfPixelSplitHits",
            "numberOfGangedPixels",
            "numberOfGangedFlaggedFakes",
            "numberOfPixelDeadSensors",
            "numberOfPixelSpoiltHits",
            "numberOfSCTHits",
            "numberOfSCTOutliers",
            "numberOfSCTHoles",
            "numberOfSCTDoubleHoles",
            "numberOfSCTSharedHits",
            "numberOfSCTDeadSensors",
            "numberOfSCTSpoiltHits",
            "numberOfTRTHits",
            "numberOfTRTOutliers",
            "numberOfTRTHoles",
            "numberOfTRTHighThresholdHits",
            "numberOfTRTHighThresholdHitsTotal",
            "numberOfTRTHighThresholdOutliers",
            "numberOfTRTDeadStraws",
            "numberOfTRTTubeHits",
            "numberOfTRTXenonHits",
            "numberOfTRTSharedHits",
            "numberOfPrecisionLayers",
            "numberOfPrecisionHoleLayers",
            "numberOfPhiLayers",
            "numberOfPhiHoleLayers",
            "numberOfTriggerEtaLayers",
            "numberOfTriggerEtaHoleLayers",
            "numberOfGoodPrecisionLayers",
            "numberOfOutliersOnTrack",
            "standardDeviationOfChi2OS",
        };
        for (auto& hit : HitSummary) { ID_tracks->addVariable<uint8_t>(-1, hit); }
        ID_tracks->addVariable<float>(-1., "radiusOfFirstHit");
        ID_tracks->addVariable<float>(-1., "pixeldEdx");
        ID_tracks->addVariable<float>(-1., "eProbabilityHT");
        ID_tracks->addVariable<float>(-1., "eProbabilityComb");
    }
}  // namespace LeptonsInTaus
