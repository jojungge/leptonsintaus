#include <FourMomUtils/xAODP4Helpers.h>
#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonPerformanceCore/MetBranch.h>
#include <TreeTools/Defs.h>
#include <TreeTools/DiMuonTTVATreeTool.h>
#include <TreeTools/JetBranches.h>
#include <TreeTools/ParticleClassBranches.h>
#include <TreeTools/TrackingBranches.h>
#include <xAODMissingET/MissingETContainer.h>

namespace LeptonsInTaus {
    DiMuonTTVATreeTool::~DiMuonTTVATreeTool() = default;
    DiMuonTTVATreeTool::DiMuonTTVATreeTool(const std::string& tool_name) :
        asg::AsgTool(tool_name),
        m_is_taumu_tp(true),
        m_is_mc(false),
        m_met_container(),
        m_met_term("Final"),
        m_iff_truth_classifier(""),
        m_muo_sel_tool(""),
        m_jet_calib_tool(""),
        m_tree(nullptr),
        m_tag(nullptr),
        m_probe(nullptr),
        m_ID_tracks(nullptr),
        m_ME_tracks(nullptr) {
        declareProperty("isMC", m_is_mc);
        declareProperty("isTauMuonTP", m_is_taumu_tp);
        declareProperty("IFFTruthClassifier", m_iff_truth_classifier);
        declareProperty("MuonSelectionTool", m_muo_sel_tool);
        declareProperty("JetCalibrationTool", m_jet_calib_tool);

        declareProperty("MetContainer", m_met_container);
        declareProperty("MetTerm", m_met_term);
    }

    StatusCode DiMuonTTVATreeTool::fill(const xAOD::IParticle* tag, const xAOD::Muon* probe_mu) {
        const xAOD::MissingETContainer* met_container = nullptr;
        ATH_CHECK(evtStore()->retrieve(met_container, m_met_container));
        const xAOD::MissingET* met_obj = GetMET_obj(m_met_term, met_container);
        if (!met_obj) {
            ATH_MSG_FATAL("No " << m_met_term << " has been found in MET container " << m_met_container);
            return StatusCode::FAILURE;
        }

        (*m_tag) = tag;
        /// Fill the probe muon information
        (*m_probe) = probe_mu;

        (*m_probe_pass_lowpt) = m_muo_sel_tool->passedLowPtEfficiencyCuts(*probe_mu);
        (*m_probe_pass_highpt) = m_muo_sel_tool->passedHighPtCuts(*probe_mu);
        (*m_probe_quality) = m_muo_sel_tool->getQuality(*probe_mu);
        (*m_ID_tracks) = probe_mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
        (*m_ME_tracks) = probe_mu->trackParticle(xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle);

        const TLorentzVector inv_p4 = tag->p4() + probe_mu->p4();
        (*m_dilep_mll) = inv_p4.M();
        (*m_dilep_pt) = inv_p4.Pt();
        (*m_dilep_eta) = inv_p4.Eta();
        (*m_dilep_phi) = inv_p4.Phi();
        (*m_sum_cos_dphi) = std::cos(xAOD::P4Helpers::deltaPhi(probe_mu, met_obj)) + std::cos(xAOD::P4Helpers::deltaPhi(tag, met_obj));
        (*m_dilep_deta) = std::abs(tag->eta() - probe_mu->eta());
        (*m_dilep_dphi) = std::abs(xAOD::P4Helpers::deltaPhi(tag, probe_mu));
        (*m_probe_mt) = CalcMt(probe_mu, met_obj);
        if (!m_tree->fill()) { return StatusCode::FAILURE; }
        return StatusCode::SUCCESS;
    }

    StatusCode DiMuonTTVATreeTool::setup_tree(std::shared_ptr<LeptonsInTauTree> tree) {
        ATH_CHECK(m_muo_sel_tool.retrieve());
        ATH_CHECK(m_jet_calib_tool.retrieve());
        if (m_tree) {
            ATH_MSG_FATAL("The tree " << m_tree->name() << " has already been associated");
            return StatusCode::FAILURE;
        }
        m_tree = tree;
        if (m_is_mc) { ATH_CHECK(m_iff_truth_classifier.retrieve()); }

        const std::string b_tag_wp = m_jet_calib_tool->GetBTagWPs()[0];
        if (m_is_mc) {
            tree->addBranch(
                std::make_shared<MuonTPBTagSFBranch>(tree->common_tree(), m_jet_calib_tool, m_jet_calib_tool->collectionFlag(), b_tag_wp));
            tree->addBranch(std::make_shared<MuonTPJvtSFBanch>(tree->common_tree(), m_jet_calib_tool, m_jet_calib_tool->collectionFlag()));
        }
        tree->addBranch(
            std::make_shared<NumBJetBranch>(tree->common_tree(), m_jet_calib_tool->collectionFlag(), m_jet_calib_tool, b_tag_wp));
        tree->addBranch(std::make_shared<NumJetBranch>(*tree, m_jet_calib_tool->collectionFlag(), m_jet_calib_tool));
        // tree->addBranch(std::make_shared<HasBadJetBranch>(*tree,m_jet_calib_tool->collectionFlag(), m_jet_calib_tool));
        /// MET output
        tree->addBranch(std::make_shared<MissingETBranch>(*tree, m_met_container, m_met_term, "MetTST"));

        m_probe = std::make_shared<IParticleFourMomBranch>(tree->tree(), "probe");
        m_probe->disableContainerIdx();
        m_probe->addVariable(std::make_shared<ChargeBranch>(tree->tree(), m_probe->name()));
        m_probe->addVariable(std::make_shared<PtErrBranch>(tree->tree(), m_probe->name()));
        m_probe->addVariable(std::make_shared<D0SigBranch>(*tree, m_probe->name()));
        m_probe->addVariable(std::make_shared<Z0Branch>(*tree, m_probe->name()));
        m_probe->addVariable(std::make_shared<CovarianceBranch>(tree->tree(), m_probe->name()));

        /// Add the calibrated ID and MS pt
        m_probe->addVariable<float>(0, "calib_ptID", "InnerDetectorPt");
        m_probe->addVariable<float>(0, "calib_ptMS", "MuonSpectrometerPt");

        m_probe->addVariable<uint16_t>("allAuthors");
        m_probe->addVariable<uint16_t>("author");
        // m_probe->addVariable<uint16_t>(-1, "type", "muonType");
        // m_probe->addVariable<uint8_t>("quality");
        m_probe->addVariable<int>("CaloMuonIDTag");

        for (auto& hit : {// Precision
                          "numberOfPrecisionLayers", "numberOfPrecisionHoleLayers", "numberOfGoodPrecisionLayers",
                          // Close
                          "innerClosePrecisionHits", "middleClosePrecisionHits", "outerClosePrecisionHits", "extendedClosePrecisionHits",
                          // InnerOut
                          "innerOutBoundsPrecisionHits", "middleOutBoundsPrecisionHits", "outerOutBoundsPrecisionHits",
                          "extendedOutBoundsPrecisionHits", "combinedTrackOutBoundsPrecisionHits",
                          // isGood
                          "isEndcapGoodLayers", "isSmallGoodSectors",
                          // Phi
                          "numberOfPhiLayers", "numberOfPhiHoleLayers",
                          // Trigger
                          "numberOfTriggerEtaLayers", "numberOfTriggerEtaHoleLayers",
                          // Inner
                          "innerSmallHits", "innerLargeHits", "innerSmallHoles", "innerLargeHoles",
                          // Middle
                          "middleSmallHits", "middleLargeHits", "middleSmallHoles", "middleLargeHoles",
                          // Outer
                          "outerSmallHits", "outerLargeHits", "outerSmallHoles", "outerLargeHoles",
                          // Extended
                          "extendedSmallHits", "extendedLargeHits", "extendedSmallHoles", "extendedLargeHoles",
                          // this is needed for checks of the new LowPt WP
                          "cscUnspoiledEtaHits"}) {
            m_probe->addVariable<uint8_t>(-1, hit);
        }

        std::vector<std::string> MuonParameters{
            "CaloLRLikelihood",
            "EnergyLoss",
            "MeasEnergyLoss",
            "ParamEnergyLoss",
            "momentumBalanceSignificance",
            "scatteringCurvatureSignificance",
            "scatteringNeighbourSignificance",
            "segmentDeltaEta",
            "segmentDeltaPhi",
        };
        for (auto& var : MuonParameters) { m_probe->addVariable<float>(-1, var); }

        if (m_is_mc) {
            m_probe->addVariable(std::make_shared<LeptonsInTaus::IFFClassBranch>(m_probe->tree(), m_probe->name(), m_iff_truth_classifier));
        }
        std::vector<std::string> IsoVars{
            "neflowisol20",
            "ptcone20",
            "ptcone20_TightTTVA_pt1000",
            "ptcone20_TightTTVA_pt500",
            "ptvarcone30",
            "ptvarcone30_TightTTVA_pt1000",
            "ptvarcone30_TightTTVA_pt500",
            "topoetcone20",
        };
        for (auto& iso : IsoVars) { m_probe->addVariable<float>(iso); }

        tree->addBranch(m_probe);

        m_tag = std::make_shared<IParticleFourMomBranch>(tree->tree(), "tag");
        m_tag->disableContainerIdx();
        m_tag->addVariable(std::make_shared<ChargeBranch>(tree->tree(), m_tag->name()));
        m_tag->addVariable(std::make_shared<D0SigBranch>(*tree, m_tag->name()));
        m_tag->addVariable(std::make_shared<Z0Branch>(*tree, m_tag->name()));
        if (!m_is_taumu_tp && m_is_mc) {
            m_tag->addVariable(std::make_shared<IFFClassBranch>(m_tag->tree(), m_tag->name(), m_iff_truth_classifier));
        } else if (m_is_taumu_tp) {
            m_tag->addVariable(std::make_shared<ProngnessBranch>(m_tag->tree(), m_tag->name()));
            // m_tag->addVariable<float>("JetRNN","RNNJetScoreSigTrans");
            m_tag->addVariable<float>("JetRNN", "RNNJetScore");
            m_tag->addVariable<float>("EleBDT", "BDTEleScoreSigTrans_retuned");
            m_tag->addVariable<unsigned short>("quality", "JetQuality");
            // m_tag->storeMass();
            if (m_is_mc) {
                m_tag->addVariable(std::make_shared<MCTruthClassifierBranch>(tree->tree(), m_tag->name()));
                m_tag->addVariable<float>("ScaleFactor");
            }
        }

        tree->addBranch(m_tag);

        m_ID_tracks = std::make_shared<TrackBranch>(*tree, "probe_IDTrk");
        m_ME_tracks = std::make_shared<TrackBranch>(*tree, "probe_MSTrk");
        m_ME_tracks->disable_vertexing();

        configure_id_tracks(m_ID_tracks);
        tree->addBranch(m_ID_tracks);
        tree->addBranch(m_ME_tracks);

        m_dilep_mll = make_branch<float>("dilep_mll");
        m_dilep_pt = make_branch<float>("dilep_pt");
        m_dilep_eta = make_branch<float>("dilep_eta");
        m_dilep_phi = make_branch<float>("dilep_phi");
        m_dilep_dphi = make_branch<float>("dilep_dphi");
        m_dilep_deta = make_branch<float>("dilep_deta");
        m_sum_cos_dphi = make_branch<float>("dilep_SumCosDPhi");
        m_probe_mt = make_branch<float>("probe_mt");
        m_probe_pass_lowpt = make_branch<bool>("probe_passLowPt");
        m_probe_pass_highpt = make_branch<bool>("probe_passHighPt");
        m_probe_quality = make_branch<unsigned short>("probe_quality");
        return StatusCode::SUCCESS;
    }

}  // namespace LeptonsInTaus
