#ifndef ELECTRONANALYSIS_DEFS_H
#define ELECTRONANALYSIS_DEFS_H

#include <xAODEgamma/Electron.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticle.h>
#include <xAODTracking/Vertex.h>
namespace LeptonsInTaus {}
#endif
