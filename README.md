LeptonsInTaus
=============
Framework to run the first step of the identification studies to separate electrons and muons from prmopt gauge boson decays from leptons originating from tau lepton decays. The code runs on the primary `xAODs` and writes a flat n-tuple containing the most relevant information associated to the electrons and muons.

Sidemark: Electrons are missing for the moment

How to clone the repository
---------------------------
The code is based on the `MuonPerformanceAnalysis` framework developed in the context of muon efficiency and calibration studies. In order to clone the repository generate a private ssh key and make sure that it is added to your [GitLab keys](https://gitlab.cern.ch/profile/keys). Then execute the following commands
```
mkdir MyTestArea
cd MyTestArea
git clone --recursive ssh://git@gitlab.cern.ch:7999/jojungge/leptonsintaus.git source
```
This command will download the primary code and all other packages on which the code depends. The extra `source` argument renames  the cloned repository locally, which is just a common style convention in most cases. Alternatively you can also use `Kerberos` authentification method for cloning your repository, but make then sure to exchange the `GitLab` URL properly. To my taste `Kerberos` is less convenient as you have always to type in your password if you want to commit changes to the repository.

How to compile
--------------
As any code in `ATLAS` the compilation of the code is based on the `CMake` build system working with the `ATLAS` software. For the compilation of the code please first login to a node having access to the ATLAS software, e.g. `lxplus` or `mppui4.t2.rzg.mpg.de` and execute the following sequence of commands 
```
cd MyTestArea/
mkdir build/
cd build/
asetup AthDerivation,21.2.100.0,here
cmake ../source/
make -j 64
source ../build/x86_64-centos7-gcc62-opt/setup.sh
```
The first two lines are changing to the area in which you've cloned the code from git and creating the `build` directory. 
The latter is used to configure the compilation and finally to create the binaries of your compiled programme. 
The fourth line loads the `ATLAS` software into the environment of your computer, i.e. it makes the code available for execution. 
There are many flavours of the `ATLAS` software depending on the purpose each built from the central [athena](https://gitlab.cern.ch/atlas/athena) code repository. 
The flavours differ in the features available. 
For instance, `Athena` contains components to load the detector geometry or magnetic fields and also to access each particular detector modul in order to reconstruct the full event. 
But is rather heavy and hence slow. In contrast, `AthAnalysis` only has everything required for data analysis purposes. 
The `AthDerivation` releases are used to run the [DerivationFramework](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DerivationFramework). 
This framework creates skimmed copies from the primary reconstructed data and also fixes bugs made in the reconstruction software.
The `ATLAS` software is not a static construct once written and then sitting there static for all time but it has been under static development. 
New analysis techniques, object calibrations are added or bugs in the code are removed in each new release encoded in the sequence of numbers. 
The last argument saves a snippet of the setup you've used into the local folder.
In the firth line the `CMake` build system is configured to compile the software against the libraries and tools provided by the version of the ATLAS software in use and the next actually compiles your code. 
Everytime you have changed something in your `C++` code, you must come back to the build folder and compile the changes via `make`. 
If you have added new source files, then you'll have to call `cmake` as well.
Finally, the computer has to know where to find the freshly compiled libraries and tools which is configured in the last line.

Everytime you login again to the computing centre, you will start with a fresh environment knowing nothing about what you've done in previous sessions. 
It is quite useful as it allows you to simultaenously work on different projects without reinstalling the computer from scratch. In order to reload the environment from the last session please do:
```
cd MyTestArea/build/
asetup --restore
source ../build/x86_64-centos7-gcc62-opt/setup.sh
```

How to run 
----------
The code runs inside the `athena` framework. The used tools are steered via jobOptions which can be found in the [job options folder](https://gitlab.cern.ch/jojungge/leptonsintaus/-/tree/master/TreeTools/share).
It is actually intended to run it on the computing grid, but before that you should always test the code locally avoiding massive crashes. 
Make sure to execute the code in a folder which is *NOT* beneath the build folder. 
```
cd ../
mkdir run/
cd run
athena --filesInput /ptmp/mpp/junggjo9/Datasets/mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.recon.AOD.e3601_s3126_r9364/AOD.17528279._000202.pool.root.1 
LeptonsInTaus/runTTVATreeMaker.py
```





