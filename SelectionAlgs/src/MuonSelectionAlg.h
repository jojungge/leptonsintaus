#ifndef MuonSelectionAlg_H
#define MuonSelectionAlg_H
#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <xAODMuon/Muon.h>
namespace CP {
    class IMuonSelectionTool;
    class IIsolationSelectionTool;
}  // namespace CP

namespace Trig {
    class IMatchingTool;
}

class MuonSelectionAlg : public AthAlgorithm {
public:
    /// Constructor with parameters:
    MuonSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~MuonSelectionAlg();

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    bool is_matched(const xAOD::Muon* muon) const;

    ToolHandle<CP::IMuonSelectionTool> m_sel_tool;
    ToolHandle<CP::IIsolationSelectionTool> m_iso_tool;
    ToolHandle<Trig::IMatchingTool> m_matching_tool;
    std::vector<std::string> m_triggers;
    std::string m_in_cointainer;
    std::string m_out_container;
    float m_pt_cut;
    float m_eta_cut;

    float m_d0_sig_cut;
    float m_z0_sinT_cut;
};
#endif
