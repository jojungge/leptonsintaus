#ifndef MUONTREEWRITERALG_H_
#define MUONTREEWRITERALG_H_

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>

#include <memory>

#include "TreeWriter.h"

namespace LeptonsInTaus {
    class TrackBranch;
}  // namespace LeptonsInTaus

class IParticleFourMomBranch;
class MuonTreeWriter : public TreeWriter {
public:
    /// Constructor with parameters:
    MuonTreeWriter(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~MuonTreeWriter();

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    /// Muon container to pick
    std::string m_muon_container;
    bool m_do_prompt_lepton;

    std::shared_ptr<IParticleFourMomBranch> m_muon_information;
    /// Primary vertex information
    std::shared_ptr<LeptonsInTaus::VertexBranch> m_prim_vtx_info;
    std::shared_ptr<LeptonsInTaus::TrackBranch> m_ID_tracks;
    std::shared_ptr<LeptonsInTaus::TrackBranch> m_ME_tracks;
    std::shared_ptr<LeptonsInTaus::TrackBranch> m_CB_tracks;
};

#endif
