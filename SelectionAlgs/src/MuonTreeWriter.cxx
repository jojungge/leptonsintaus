#include "MuonTreeWriter.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/Property.h>
#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonPerformanceCore/MuonEventInfoBranch.h>
#include <TreeTools/Defs.h>
#include <TreeTools/JetBranches.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TreeTools/ParticleClassBranches.h>
#include <TreeTools/TrackingBranches.h>
#include <xAODMuon/MuonContainer.h>

MuonTreeWriter::MuonTreeWriter(const std::string& name, ISvcLocator* pSvcLocator) :
    TreeWriter(name, pSvcLocator),
    m_muon_container(""),
    m_do_prompt_lepton(false),
    m_muon_information(nullptr),
    m_prim_vtx_info(nullptr),
    m_ID_tracks(nullptr),
    m_ME_tracks(nullptr),
    m_CB_tracks(nullptr) {
    declareProperty("MuonContainer", m_muon_container);
    declareProperty("doPromptLepton", m_do_prompt_lepton);
}
MuonTreeWriter::~MuonTreeWriter() {}
//**********************************************************************
StatusCode MuonTreeWriter::initialize() {
    ATH_CHECK(create_tree());
    if (!m_tree->addBranch(std::make_shared<LeptonsInTaus::VertexBranch>("PrimVtx", *m_tree, true))) return StatusCode::FAILURE;
    m_prim_vtx_info = m_tree->get_branch<LeptonsInTaus::VertexBranch>("PrimVtx");

    m_muon_information = std::make_shared<IParticleFourMomBranch>(m_tree->tree(), "Muons");

    /// Add the calibrated ID and MS pt
    m_muon_information->addVariable<float>(0, "calib_ptID", "InnerDetectorPt");
    m_muon_information->addVariable<float>(0, "calib_ptMS", "MuonSpectrometerPt");

    m_muon_information->addVariable<uint16_t>("allAuthors");
    m_muon_information->addVariable<uint16_t>("author");
    // m_muon_information->addVariable<uint16_t>(-1, "type", "muonType");
    m_muon_information->addVariable<uint8_t>("quality");
    m_muon_information->addVariable<int>("CaloMuonIDTag");

    for (auto& hit : {// Precision
                      "numberOfPrecisionLayers", "numberOfPrecisionHoleLayers", "numberOfGoodPrecisionLayers",
                      // Close
                      "innerClosePrecisionHits", "middleClosePrecisionHits", "outerClosePrecisionHits", "extendedClosePrecisionHits",
                      // InnerOut
                      "innerOutBoundsPrecisionHits", "middleOutBoundsPrecisionHits", "outerOutBoundsPrecisionHits",
                      "extendedOutBoundsPrecisionHits", "combinedTrackOutBoundsPrecisionHits",
                      // isGood
                      "isEndcapGoodLayers", "isSmallGoodSectors",
                      // Phi
                      "numberOfPhiLayers", "numberOfPhiHoleLayers",
                      // Trigger
                      "numberOfTriggerEtaLayers", "numberOfTriggerEtaHoleLayers",
                      // Inner
                      "innerSmallHits", "innerLargeHits", "innerSmallHoles", "innerLargeHoles",
                      // Middle
                      "middleSmallHits", "middleLargeHits", "middleSmallHoles", "middleLargeHoles",
                      // Outer
                      "outerSmallHits", "outerLargeHits", "outerSmallHoles", "outerLargeHoles",
                      // Extended
                      "extendedSmallHits", "extendedLargeHits", "extendedSmallHoles", "extendedLargeHoles",
                      // this is needed for checks of the new LowPt WP
                      "cscUnspoiledEtaHits"}) {
        m_muon_information->addVariable<uint8_t>(-1, hit);
    }

    std::vector<std::string> MuonParameters{
        "CaloLRLikelihood",
        "EnergyLoss",
        "MeasEnergyLoss",
        "ParamEnergyLoss",
        "momentumBalanceSignificance",
        "scatteringCurvatureSignificance",
        "scatteringNeighbourSignificance",
        "segmentDeltaEta",
        "segmentDeltaPhi",
    };
    for (auto& var : MuonParameters) { m_muon_information->addVariable<float>(var); }

    if (is_mc()) {
        m_muon_information->addVariable(std::make_shared<LeptonsInTaus::IFFClassBranch>(
            m_muon_information->tree(), m_muon_information->name(), m_iff_truth_classifier));
        m_muon_information->addVariable(
            std::make_shared<LeptonsInTaus::TauMarkerBranch>(m_muon_information->tree(), m_muon_information->name() + "_fromTauDecay"));
    }
    std::vector<std::string> IsoVars{
        "neflowisol20",
        "ptcone20",
        "ptcone20_TightTTVA_pt1000",
        "ptcone20_TightTTVA_pt500",
        "ptvarcone30",
        "ptvarcone30_TightTTVA_pt1000",
        "ptvarcone30_TightTTVA_pt500",
        "topoetcone20",
    };
    for (auto& iso : IsoVars) { m_muon_information->addVariable<float>(iso); }

    if (m_do_prompt_lepton) {
        static const std::vector<std::string> tagging_short_vars{//"PromptLeptonInput_TrackJetNTrack", "PromptLeptonInput_sv1_jf_ntrkv",
                                                                 "PromptLeptonImprovedInput_MVAXBin"};
        for (const auto& var : tagging_short_vars) { m_muon_information->addVariable<short>(-10000, var, var); }
        static const std::vector<std::string> tagging_float_vars{
            "PromptLeptonInput_DL1mu",
            //"PromptLeptonInput_DRlj",
            "PromptLeptonInput_LepJetPtFrac",
            "PromptLeptonInput_ip2",
            "PromptLeptonInput_ip3",
            "PromptLeptonInput_rnnip",
            "PromptLeptonImprovedInput_PtFrac",
            "PromptLeptonImprovedInput_DRlj",
            "PromptLeptonRNN_prompt",
            "PromptLeptonRNN_non_prompt_b",
            "PromptLeptonRNN_non_prompt_c",
            "PromptLeptonImprovedInput_CaloClusterERel",
            "PromptLeptonImprovedInput_CandVertex_normDistToPriVtxLongitudinalBest",
        };
        for (const auto& var : tagging_float_vars) { m_muon_information->addVariable<float>(-10000.0, var, var); }
    }
    if (!m_jet_calib_tool.empty()) {
        m_muon_information->addVariable(std::make_shared<LeptonsInTaus::ClosestJetBranch>(
            m_tree->tree(), "closest_" + m_jet_calib_tool->collectionFlag(), m_jet_calib_tool));
    }
    m_tree->addBranch(m_muon_information);
    m_ID_tracks = std::make_shared<LeptonsInTaus::TrackBranch>(*m_tree, "IDTracks");
    m_ME_tracks = std::make_shared<LeptonsInTaus::TrackBranch>(*m_tree, "MSTracks");
    m_CB_tracks = std::make_shared<LeptonsInTaus::TrackBranch>(*m_tree, "CBTracks");
    m_ME_tracks->disable_vertexing();
    m_CB_tracks->disable_vertexing();
    LeptonsInTaus::configure_id_tracks(m_ID_tracks);

    if (!m_tree->addBranch(m_ID_tracks)) return StatusCode::FAILURE;
    if (!m_tree->addBranch(m_ME_tracks)) return StatusCode::FAILURE;
    if (!m_tree->addBranch(m_CB_tracks)) return StatusCode::FAILURE;

    /// Initialize the trees
    ATH_CHECK(initialize_tree());
    return StatusCode::SUCCESS;
}
StatusCode MuonTreeWriter::execute() {
    if (!m_tree->get_prim_vtx()) {
        ATH_MSG_DEBUG("Skip event");
        m_tree->release_common_containers();
        return StatusCode::SUCCESS;
    }

    const xAOD::MuonContainer* muons = nullptr;
    ATH_CHECK(evtStore()->retrieve(muons, m_muon_container));
    // No muons in the event... So give up
    m_prim_vtx_info->set_vertex(m_tree->get_prim_vtx());
    /// Muon selection made by the previously scheduled MuonSelectionAlg
    for (const auto& mu : *muons) {
        m_prim_vtx_info->set_vertex(m_tree->get_prim_vtx());
        (*m_muon_information) = mu;
        (*m_ID_tracks) = mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
        (*m_ME_tracks) = mu->trackParticle(xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle);
        (*m_CB_tracks) = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);

        if (!m_tree->fill()) return StatusCode::FAILURE;
    }
    m_tree->release_common_containers();
    return StatusCode::SUCCESS;
}
