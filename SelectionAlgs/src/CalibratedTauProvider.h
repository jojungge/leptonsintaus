#ifndef CalibratedTauProvider_H
#define CalibratedTauProvider_H

#include <GaudiKernel/ToolHandle.h>
#include <MuonPerformanceAlgs/ShallowCopyMakerAlg.h>
#include <TauAnalysisTools/ITauSmearingTool.h>

class CalibratedTauProvider : public ShallowCopyMakerAlgorithm {
public:
    /// Constructor with parameters:
    CalibratedTauProvider(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~CalibratedTauProvider() = default;

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    ToolHandle<TauAnalysisTools::ITauSmearingTool> m_calib_tool;
};
#endif
