#include "CalibratedTauProvider.h"

#include <xAODTau/TauJetContainer.h>

CalibratedTauProvider::CalibratedTauProvider(const std::string& name, ISvcLocator* pSvcLocator) :
    ShallowCopyMakerAlgorithm(name, pSvcLocator), m_calib_tool("") {
    declareProperty("CalibrationTool", m_calib_tool);
}
StatusCode CalibratedTauProvider::initialize() {
    ATH_CHECK(ShallowCopyMakerAlgorithm::initialize());
    ATH_CHECK(m_calib_tool.retrieve());
    return StatusCode::SUCCESS;
}
StatusCode CalibratedTauProvider::execute() {
    ATH_CHECK(makeShallowCopy<xAOD::TauJetContainer>());
    xAOD::TauJetContainer* container = nullptr;
    ATH_CHECK(evtStore()->retrieve(container, out_container()));
    for (xAOD::TauJet* tau : *container) {
        if (m_calib_tool->applyCorrection(*tau) != CP::CorrectionCode::Ok) {
            ATH_MSG_FATAL("Failed to calibrate tau with " << tau->pt() / 1.e3 << " GeV, " << tau->eta() << ", " << tau->phi());
            return StatusCode::FAILURE;
        }
    }
    return StatusCode::SUCCESS;
}
