#include "DiMuonTPSelAlg.h"

#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonPerformanceCore/MetBranch.h>
#include <MuonPerformanceCore/MuonTriggerBranches.h>
#include <MuonTPInterfaces/IMuonJetCalibTool.h>
#include <TreeTools/ITPMuonTTVATreeTool.h>
#include <TreeTools/JetBranches.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <xAODMuon/MuonContainer.h>

DiMuonTPSelAlg::DiMuonTPSelAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    TreeWriter(name, pSvcLocator),
    m_tag_container(""),
    m_probe_container(""),
    m_tree_tool(""),
    m_min_mll(61.2 * 1.e3),
    m_max_mll(121.2 * 1.e3),
    m_accept_oc(true),
    m_accept_sc(false),
    //
    m_veto_bjets(true),
    m_require_bjets(false),
    m_bjet_accessor(nullptr),
    m_triggers(),
    m_trigger_tool(""),
    m_trig_match_tool("") {
    declareProperty("TagContainer", m_tag_container);
    declareProperty("ProbeContainer", m_probe_container);

    declareProperty("TreeTool", m_tree_tool);
    declareProperty("AcceptOC", m_accept_oc);
    declareProperty("AcceptSC", m_accept_sc);

    declareProperty("VetoBJets", m_veto_bjets);
    declareProperty("RequireBJets", m_require_bjets);

    /// Invariant mass selection cuts
    declareProperty("LowMassCut", m_min_mll);
    declareProperty("HighMassCut", m_max_mll);

    declareProperty("Triggers", m_triggers);
    declareProperty("TriggerToolMuon", m_trigger_tool);
    declareProperty("TriggerMatchingTool", m_trig_match_tool);
}

DiMuonTPSelAlg::~DiMuonTPSelAlg() = default;

StatusCode DiMuonTPSelAlg::initialize() {
    if (!m_accept_oc && !m_accept_sc) {
        ATH_MSG_FATAL("If no same and oppsite charge events are accepted then where is the sense of running this alg?");
        return StatusCode::FAILURE;
    }
    if (m_tag_container.empty() && m_probe_container.empty()) {
        ATH_MSG_FATAL("No muon container has been defined");
        return StatusCode::FAILURE;
    }
    if (m_tag_container.empty()) { m_tag_container = m_probe_container; }
    if (m_probe_container.empty()) { m_probe_container = m_tag_container; }
    if (m_min_mll > m_max_mll) {
        ATH_MSG_FATAL("Invalid configuration of the invariant mass window: " << m_min_mll << " - " << m_max_mll);
        return StatusCode::FAILURE;
    }
    ATH_CHECK(create_tree());
    /// Check the jet configuration
    ATH_CHECK(m_jet_calib_tool.retrieve());
    if (m_jet_calib_tool->GetBTagWPs().size() > 1) {
        ATH_MSG_FATAL("More than two b-tagging working points are configured. Only one is accepted");
        return StatusCode::FAILURE;
    } else if (m_jet_calib_tool->GetBTagWPs().empty()) {
        m_require_bjets = m_veto_bjets = false;
    } else {
        const std::string b_tag_wp = m_jet_calib_tool->GetBTagWPs()[0];
        m_bjet_accessor = std::make_unique<BoolAccessor>("isBtag_" + b_tag_wp);
    }

    ATH_CHECK(m_tree_tool.retrieve());
    ATH_CHECK(m_tree_tool->setup_tree(m_tree));
    /// Trigger initialization
    if (!m_triggers.empty()) {
        ATH_CHECK(m_trigger_tool.retrieve());
        ATH_CHECK(m_trig_match_tool.retrieve());
        for (const std::string& trig_item : m_triggers) {
            m_tree->addBranch(std::make_shared<TriggerDecisionBranch>(m_tree->common_tree(), m_trigger_tool, trig_item));
            if (!is_mc()) { m_tree->addBranch(std::make_shared<TriggerPreScaleBranch>(m_tree->common_tree(), m_trigger_tool, trig_item)); }
        }
    }
    ATH_CHECK(initialize_tree());
    return StatusCode::SUCCESS;
}

bool DiMuonTPSelAlg::pass_charge_sel(const int charge_prod) const {
    if (charge_prod < 0 && m_accept_oc)
        return true;
    else if (charge_prod > 0 && m_accept_sc)
        return true;
    return false;
}
bool DiMuonTPSelAlg::pass_trigger() {
    if (m_triggers.empty()) return true;
    for (const std::string& trigger : m_triggers) {
        if (m_trigger_tool->TrigDecision(trigger)) {
            ATH_MSG_DEBUG("Trigger " << trigger << " fired. The event is reasonable");
            return true;
        }
    }
    return false;
}
StatusCode DiMuonTPSelAlg::execute() {
    xAOD::MuonContainer* probes = nullptr;
    xAOD::MuonContainer* tags = nullptr;

    ATH_CHECK(evtStore()->retrieve(probes, m_probe_container));
    ATH_CHECK(evtStore()->retrieve(tags, m_tag_container));
    if (probes->empty() || tags->empty()) {
        ATH_MSG_DEBUG("Found either an empty tag muon container " << tags->size() << " or an empty probe muon container " << probes->size()
                                                                  << ". Terminate method");

        return StatusCode::SUCCESS;
    }
    if (!m_tree->get_prim_vtx()) {
        ATH_MSG_DEBUG("No valid vertex has been found");
        return StatusCode::SUCCESS;
    }
    // next check the triggers
    if (!pass_trigger()) {
        ATH_MSG_DEBUG("No suitable trigger has been fired. Remove the event");
        return StatusCode::SUCCESS;
    }
    /// Check whether the b-jet selection is satified
    if (!pass_jet_selection()) {
        ATH_MSG_DEBUG("Event fails jet requirement. Remove it");
        return StatusCode::SUCCESS;
    }
    m_tree->release_common_containers();
    for (xAOD::Muon* tag_mu : *tags) {
        for (xAOD::Muon* probe_mu : *probes) {
            if (IsSame(probe_mu, tag_mu)) continue;
            const xAOD::TrackParticle* id_track = probe_mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
            const xAOD::TrackParticle* cb_track = probe_mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
            const xAOD::TrackParticle* ms_track =
                probe_mu->trackParticle(xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle);
            if (!id_track || !cb_track || !ms_track) continue;
            /// Charge selection
            const int charge_prod = probe_mu->charge() * tag_mu->charge();
            if (!pass_charge_sel(charge_prod)) continue;
            /// Invariant mass cut
            const TLorentzVector inv_p4 = probe_mu->p4() + tag_mu->p4();
            if (inv_p4.M() < m_min_mll || inv_p4.M() > m_max_mll) continue;
            ATH_CHECK(m_tree_tool->fill(tag_mu, probe_mu));
        }
    }
    m_tree->release_common_containers();
    return StatusCode::SUCCESS;
}

bool DiMuonTPSelAlg::pass_jet_selection() {
    if (m_jet_calib_tool->numberOfBadJets())
        return false;
    else if (m_veto_bjets == m_require_bjets)
        return true;
    unsigned int n_b_jets = 0;
    for (const xAOD::Jet* sel_jet : *m_jet_calib_tool->GetJets()) {
        ATH_MSG_DEBUG("Check jet " << sel_jet->pt() / 1.e3 << " GeV, eta:" << sel_jet->eta() << ", phi: " << sel_jet->phi()
                                   << " isB: " << (*m_bjet_accessor)(*sel_jet));
        n_b_jets += (*m_bjet_accessor)(*sel_jet);
    }

    return (m_veto_bjets && !n_b_jets) || (m_require_bjets && n_b_jets);
}
