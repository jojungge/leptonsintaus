#ifndef StonjekTaus_H
#define StonjekTaus_H

#include <GaudiKernel/ToolHandle.h>
#include <MuonPerformanceAlgs/ShallowCopyMakerAlg.h>
#include <TauAnalysisTools/ITauSmearingTool.h>

class StonjekTaus : public ShallowCopyMakerAlgorithm {
public:
    /// Constructor with parameters:
    StonjekTaus(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~StonjekTaus() = default;

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;
    StatusCode finalize() override;

private:
    std::string m_tau_container;
    std::string m_muon_container;

    unsigned int m_ev;
    unsigned int m_mu_tau_pairs;
    unsigned int m_di_mu_pairs;
    unsigned int m_di_tau_pairs;
};
#endif
