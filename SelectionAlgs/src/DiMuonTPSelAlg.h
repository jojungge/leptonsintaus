#ifndef DiMuonTPSelAlg_H
#define DiMuonTPSelAlg_H

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <MuonTPTools/Utils.h>

#include "TreeWriter.h"

class IMuonTPTrigUtils;
class IMuonJetCalibTool;
class IParticleFourMomBranch;
namespace Trig {
    class IMatchingTool;
}

namespace LeptonsInTaus {
    class ITPMuonTTVATreeTool;

}
class DiMuonTPSelAlg : public TreeWriter {
public:
    /// Constructor with parameters:
    DiMuonTPSelAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~DiMuonTPSelAlg();

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    bool pass_charge_sel(const int charge_prod) const;

    bool pass_jet_selection();

    bool pass_trigger();

    /// Muon container in the store gate
    std::string m_tag_container;
    /// Tau container in the store gate
    std::string m_probe_container;

    ToolHandle<LeptonsInTaus::ITPMuonTTVATreeTool> m_tree_tool;
    /// Mass window cuts
    float m_min_mll;
    float m_max_mll;

    /// Relative charge cut
    bool m_accept_oc;
    bool m_accept_sc;

    bool m_veto_bjets;
    bool m_require_bjets;
    std::unique_ptr<BoolAccessor> m_bjet_accessor;

    /// Trigger selection
    std::vector<std::string> m_triggers;
    ToolHandle<IMuonTPTrigUtils> m_trigger_tool;
    ToolHandle<Trig::IMatchingTool> m_trig_match_tool;
};
#endif
