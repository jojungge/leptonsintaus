#ifndef METMakerAlg_H
#define METMakerAlg_H

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <METInterface/IMETMaker.h>
#include <METInterface/IMETSystematicsTool.h>
#include <MuonTPInterfaces/IMuonJetCalibTool.h>
#include <MuonTPTools/Utils.h>
#include <xAODMissingET/MissingETContainer.h>

#include <memory>

class METMakerAlg : public AthAlgorithm {
public:
    /// Constructor with parameters:
    METMakerAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~METMakerAlg() = default;

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    StatusCode CreateContainer(const std::string& cont_name, xAOD::MissingETContainer*& Cont);

    std::string referenceTerm(xAOD::Type::ObjectType Type) const;

    StatusCode addContainerToMet(xAOD::MissingETContainer* MET, const xAOD::IParticleContainer* particles, xAOD::Type::ObjectType type,
                                 const xAOD::IParticleContainer* invisible);

    StatusCode markInvisible(xAOD::MissingETContainer* MET, const xAOD::IParticleContainer* invisible);

    StatusCode buildMET(xAOD::MissingETContainer* MET, const std::string& softTerm, bool doJvt, const xAOD::IParticleContainer* invisible,
                        bool build_track = false);
    std::string m_elec_container;
    std::string m_muon_container;
    std::string m_tau_container;

    std::string m_out_container;

    ToolHandle<IMuonJetCalibTool> m_jet_calib_tool;
    ToolHandle<IMETMaker> m_metMaker;
    ToolHandle<IMETSystematicsTool> m_metSystTool;
    /// Name of the xAOD container
    std::string m_met_key;
    /// Name of the xAOD association map
    std::string m_met_map_key;

    std::string m_EleRefTerm;
    std::string m_MuoRefTerm;
    std::string m_TauRefTerm;
    std::string m_JetRefTerm;
    std::string m_PhoRefTerm;

    std::string m_TrackSoftTerm;
    std::string m_CaloSoftTerm;

    std::string m_FinalMetTerm;
    std::string m_FinalTrackTerm;

    bool m_trkJetsyst;
    bool m_trkMETsyst;

    bool m_isMC;
};
#endif
