#include "TauSelectionAlg.h"

#include <MuonTPTools/Utils.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>
namespace {
    FloatDecorator dec_SF("ScaleFactor");
    SG::AuxElement::Decorator<unsigned short> dec_quality("JetQuality");

    constexpr float MinMuonPt = 3.e3;
}  // namespace
TauSelectionAlg::TauSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_sel_tool(""),
    m_reco_eff_tool(""),
    m_matching_tool(""),
    m_in_cointainer(""),
    m_out_container(""),
    m_triggers(),
    m_muon_container(""),
    m_tau_muon_or(0.),
    m_elec_container(""),
    m_tau_elec_or(0.) {
    declareProperty("SelectionTool", m_sel_tool);
    declareProperty("RecoEffiTool", m_reco_eff_tool);
    declareProperty("TrigMatchingTool", m_matching_tool);
    declareProperty("Triggers", m_triggers);
    declareProperty("InContainer", m_in_cointainer);
    declareProperty("OutContainer", m_out_container);

    declareProperty("MuonContainerForOR", m_muon_container);
    declareProperty("TauMuonOR", m_tau_muon_or);

    declareProperty("ElecContainerForOR", m_elec_container);
    declareProperty("TauElecOR", m_tau_elec_or);
}
StatusCode TauSelectionAlg::initialize() {
    if (m_in_cointainer.empty()) {
        ATH_MSG_FATAL("No input container has been given");
        return StatusCode::FAILURE;
    }
    if (m_out_container.empty()) {
        ATH_MSG_FATAL("Where to store the selected objects");
        return StatusCode::FAILURE;
    }
    if (m_triggers.size()) {
        ATH_CHECK(m_matching_tool.retrieve());
        ATH_MSG_INFO("Will only select taus matched to one of the listed triggers:");
        for (const auto& trig : m_triggers) { ATH_MSG_INFO("  *** " << trig); }
    }
    if (m_tau_muon_or > 0. && m_muon_container.empty()) {
        ATH_MSG_FATAL("Tau muon overlap removal defined but no muon container given");
        return StatusCode::FAILURE;
    } else if (m_tau_muon_or > 0.) {
        ATH_MSG_INFO("Taus are required to be separated by deltaR = " << m_tau_muon_or << " from muons in container " << m_muon_container);
    } else {
        m_muon_container.clear();
    }
    if (m_tau_elec_or > 0. && m_elec_container.empty()) {
        ATH_MSG_FATAL("Tau elec overlap removal defined but no electron container given");
        return StatusCode::FAILURE;
    } else if (m_tau_elec_or > 0.) {
        ATH_MSG_INFO("Taus are required to be separated by deltaR = " << m_tau_elec_or << " from electron in container "
                                                                      << m_elec_container);
    } else {
        m_elec_container.clear();
    }
    ATH_CHECK(m_sel_tool.retrieve());
    return StatusCode::SUCCESS;
}
StatusCode TauSelectionAlg::execute() {
    xAOD::TauJetContainer* in_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(in_container, m_in_cointainer));
    xAOD::TauJetContainer* out_container = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
    ATH_CHECK(evtStore()->record(out_container, m_out_container));
    for (xAOD::TauJet* tau : *in_container) {
        ATH_MSG_DEBUG("Check tau with pt: " << tau->pt() / 1.e3 << " GeV, eta: " << tau->eta() << ", phi: " << tau->phi()
                                            << " RNN Jet: " << tau->discriminant(xAOD::TauJetParameters::RNNJetScoreSigTrans)
                                            << ", BDT Ele: " << tau->discriminant(xAOD::TauJetParameters::BDTEleScore));
        if (!m_sel_tool->accept(*tau)) continue;
        if (!pass_MuonOR(tau) || !pass_ElecOR(tau)) continue;
        /// Retrieve the tau scale-factor
        if (!m_reco_eff_tool.empty()) {
            double sf = 1.;
            if (m_reco_eff_tool->getEfficiencyScaleFactor(*tau, sf) != CP::CorrectionCode::Ok) {
                ATH_MSG_WARNING("Failed to retrieve tau efficiency scale factor.");
                sf = 1.;
            }
            dec_SF(*tau) = sf;
        }

        unsigned short Quality = xAOD::Muon::VeryLoose;
        if (tau->isTau(xAOD::TauJetParameters::JetRNNSigTight)) {
            Quality = xAOD::Muon::Tight;
        } else if (tau->isTau(xAOD::TauJetParameters::JetRNNSigMedium)) {
            Quality = xAOD::Muon::Medium;
        } else if (tau->isTau(xAOD::TauJetParameters::JetRNNSigLoose)) {
            Quality = xAOD::Muon::Loose;
        }

        dec_quality(*tau) = Quality;

        if (is_matched(tau)) out_container->push_back(tau);
    }
    out_container->sort([](const xAOD::TauJet* t1, const xAOD::TauJet* t2) { return t1->pt() > t2->pt(); });
    return StatusCode::SUCCESS;
}
bool TauSelectionAlg::is_matched(const xAOD::TauJet* tau) const {
    if (m_triggers.empty()) return true;
    for (const auto& trig : m_triggers) {
        if (m_matching_tool->match(*tau, trig)) {
            ATH_MSG_DEBUG("Tau with pt " << tau->pt() / 1.e3 << " GeV, eta: " << tau->eta() << ", phi: " << tau->phi()
                                         << " matched to trigger" << trig);
            return true;
        }
    }
    return false;
}
bool TauSelectionAlg::pass_MuonOR(const xAOD::TauJet* tau) const {
    if (m_muon_container.empty()) {
        ATH_MSG_DEBUG("No muon container has been defined --> No overlap removal performed.");
        return true;
    }
    const xAOD::MuonContainer* muons = nullptr;
    if (evtStore()->retrieve(muons, m_muon_container).isFailure()) {
        ATH_MSG_FATAL("No muon container found. Reject the tau anyway");
        return false;
    }
    const float dR2 = m_tau_muon_or * m_tau_muon_or;
    for (const xAOD::Muon* muon : *muons) {
        if (muon->pt() > MinMuonPt && DeltaR2(muon, tau) < dR2) {
            ATH_MSG_DEBUG("Overlap with muon having pT: " << muon->pt() / 1.e3 << " GeV, eta: " << muon->eta() << ", phi: " << muon->phi()
                                                          << " Q: " << muon->charge() << " author: " << muon->author()
                                                          << " within dR: " << DeltaR(muon, tau) << "< " << m_tau_muon_or);
            return false;
        }
    }
    return true;
}

bool TauSelectionAlg::pass_ElecOR(const xAOD::TauJet* tau) const {
    if (m_elec_container.empty()) {
        ATH_MSG_DEBUG("No electron container has been defined --> No overlap removal performed.");
        return true;
    }
    const xAOD::ElectronContainer* electrons = nullptr;
    if (evtStore()->retrieve(electrons, m_elec_container).isFailure()) {
        ATH_MSG_FATAL("No electron container found. Reject the tau anyway");
        return false;
    }
    const float dR2 = m_tau_elec_or * m_tau_elec_or;
    for (const xAOD::Electron* elec : *electrons) {
        if (DeltaR2(elec, tau) < dR2) {
            ATH_MSG_DEBUG("Overlap with electron having pT: " << elec->pt() / 1.e3 << " GeV, eta: " << elec->eta()
                                                              << ", phi: " << elec->phi() << " Q: " << elec->charge()
                                                              << " within dR: " << DeltaR(elec, tau) << "< " << m_tau_elec_or);
            return false;
        }
    }
    return true;
}
