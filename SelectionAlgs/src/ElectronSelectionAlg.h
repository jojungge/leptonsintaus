#ifndef ElectronSelectionAlg_H
#define ElectronSelectionAlg_H

#include <AthenaBaseComps/AthAlgorithm.h>
#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <GaudiKernel/ToolHandle.h>
#include <MuonTPTools/Utils.h>
#include <TriggerMatchingTool/IMatchingTool.h>

#include <memory>
class ElectronSelectionAlg : public AthAlgorithm {
public:
    /// Constructor with parameters:
    ElectronSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~ElectronSelectionAlg() = default;

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    ToolHandle<IAsgElectronLikelihoodTool> m_sel_tool;
    bool m_use_sel_decoration;
    std::string m_decor_name;
    std::unique_ptr<CharAccessor> m_sel_acc;
    std::string m_in_cointainer;
    std::string m_out_container;

    float m_min_pt;
    bool m_exclude_calo_gap;
};
#endif
