#ifndef MuonTauTPSelAlg_H
#define MuonTauTPSelAlg_H

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <MuonTPTools/Utils.h>

#include "TreeWriter.h"

class IMuonTPTrigUtils;
class IMuonJetCalibTool;
class IParticleFourMomBranch;
class TH1;
namespace Trig {
    class IMatchingTool;
}

namespace LeptonsInTaus {
    class ITPMuonTTVATreeTool;

}
class MuonTauTPSelAlg : public TreeWriter {
public:
    /// Constructor with parameters:
    MuonTauTPSelAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~MuonTauTPSelAlg();

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    bool pass_charge_sel(const int charge_prod) const;

    bool pass_jet_selection();

    bool pass_trigger();
    /// Hist service for the communication with the output files

    /// Muon container in the store gate
    std::string m_muon_container;
    /// Electron container in the store gate --> used for veto
    std::string m_elec_container;
    /// Tau container in the store gate
    std::string m_tau_container;
    /// Missing et container in the store gate
    std::string m_met_container;

    ToolHandle<LeptonsInTaus::ITPMuonTTVATreeTool> m_tree_tool;
    /// Mass window cuts
    float m_min_mll;
    float m_max_mll;

    /// Mt windows of the muon with met
    float m_min_mt;
    float m_max_mt;

    /// Relative charge cut
    bool m_accept_oc;
    bool m_accept_sc;

    bool m_veto_bjets;
    bool m_require_bjets;
    std::unique_ptr<BoolAccessor> m_bjet_accessor;

    /// Trigger selection
    bool m_cut_on_trigger;
    std::vector<std::string> m_triggers;
    ToolHandle<IMuonTPTrigUtils> m_trigger_tool;
    ToolHandle<Trig::IMatchingTool> m_trig_match_tool;

    TH1* m_cutflow_histo;
};
#endif
