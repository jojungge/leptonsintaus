// LeptonsInTaus_entries.cxx
#include "../CalibratedTauProvider.h"
#include "../DiMuonTPSelAlg.h"
#include "../ElectronSelectionAlg.h"
#include "../METMakerAlg.h"
#include "../MuonSelectionAlg.h"
#include "../MuonTauTPSelAlg.h"
#include "../MuonTreeWriter.h"
#include "../StonjekTaus.h"
#include "../TauSelectionAlg.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
DECLARE_ALGORITHM_FACTORY(MuonTreeWriter)
DECLARE_ALGORITHM_FACTORY(CalibratedTauProvider)
DECLARE_ALGORITHM_FACTORY(TauSelectionAlg)
DECLARE_ALGORITHM_FACTORY(MuonSelectionAlg)
DECLARE_ALGORITHM_FACTORY(MuonTauTPSelAlg)
DECLARE_ALGORITHM_FACTORY(DiMuonTPSelAlg)
DECLARE_ALGORITHM_FACTORY(ElectronSelectionAlg)
DECLARE_ALGORITHM_FACTORY(StonjekTaus)
DECLARE_ALGORITHM_FACTORY(METMakerAlg)
DECLARE_FACTORY_ENTRIES(SelectionAlgs) {
    DECLARE_ALGORITHM(MuonTreeWriter);
    DECLARE_ALGORITHM(CalibratedTauProvider);
    DECLARE_ALGORITHM(TauSelectionAlg);
    DECLARE_ALGORITHM(MuonSelectionAlg);
    DECLARE_ALGORITHM(MuonTauTPSelAlg);
    DECLARE_ALGORITHM(DiMuonTPSelAlg);
    DECLARE_ALGORITHM(ElectronSelectionAlg);
    DECLARE_ALGORITHM(METMakerAlg);
    DECLARE_ALGORITHM(StonjekTaus);
}
