#ifndef TauSelectionAlg_H
#define TauSelectionAlg_H

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <TauAnalysisTools/ITauEfficiencyCorrectionsTool.h>
#include <TauAnalysisTools/ITauSelectionTool.h>
#include <TriggerMatchingTool/IMatchingTool.h>

class TauSelectionAlg : public AthAlgorithm {
public:
    /// Constructor with parameters:
    TauSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~TauSelectionAlg() = default;

    /// Athena algorithm's Hooks
    StatusCode initialize() override;
    StatusCode execute() override;

private:
    bool is_matched(const xAOD::TauJet* tau) const;
    bool pass_MuonOR(const xAOD::TauJet* tau) const;
    bool pass_ElecOR(const xAOD::TauJet* tau) const;

    ToolHandle<TauAnalysisTools::ITauSelectionTool> m_sel_tool;
    ToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool> m_reco_eff_tool;
    ToolHandle<Trig::IMatchingTool> m_matching_tool;
    std::string m_in_cointainer;
    std::string m_out_container;

    std::vector<std::string> m_triggers;

    /// Overlap removal with muons
    std::string m_muon_container;
    float m_tau_muon_or;

    std::string m_elec_container;
    float m_tau_elec_or;
};
#endif
