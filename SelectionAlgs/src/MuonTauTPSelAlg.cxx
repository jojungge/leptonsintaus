#include "MuonTauTPSelAlg.h"

#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonPerformanceCore/MetBranch.h>
#include <MuonPerformanceCore/MuonTriggerBranches.h>
#include <MuonTPInterfaces/IMuonJetCalibTool.h>
#include <TH1I.h>
#include <TreeTools/ITPMuonTTVATreeTool.h>
#include <TreeTools/JetBranches.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>
namespace {
    enum CutFlowOrder {
        AllEvents = 1,
        ElectronVeto,
        MuonSelection,
        TauSelection,
        PrimaryVtx,
        Trigger,
        JetSelection,
        MuonMtCut,
        AllMuonTracklets,
        ChargePair,
        MassCut,
        AllCuts
    };
}
MuonTauTPSelAlg::MuonTauTPSelAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    TreeWriter(name, pSvcLocator),
    m_muon_container(""),
    m_elec_container(""),
    m_tau_container(""),
    m_met_container(""),
    m_tree_tool(""),
    m_min_mll(0.),
    m_max_mll(1.e8),
    m_min_mt(0),
    m_max_mt(1.e8),
    m_accept_oc(true),
    m_accept_sc(false),
    //
    m_veto_bjets(true),
    m_require_bjets(false),
    m_bjet_accessor(nullptr),
    m_cut_on_trigger(true),
    m_triggers(),
    m_trigger_tool(""),
    m_trig_match_tool(""),
    m_cutflow_histo(nullptr) {
    declareProperty("MuonContainer", m_muon_container);
    declareProperty("TauContainer", m_tau_container);
    declareProperty("ElectronContainer", m_elec_container);
    declareProperty("METContainer", m_met_container);

    declareProperty("TreeTool", m_tree_tool);
    declareProperty("AcceptOC", m_accept_oc);
    declareProperty("AcceptSC", m_accept_sc);

    declareProperty("VetoBJets", m_veto_bjets);
    declareProperty("RequireBJets", m_require_bjets);

    /// Invariant mass selection cuts
    declareProperty("LowMassCut", m_min_mll);
    declareProperty("HighMassCut", m_max_mll);
    declareProperty("LowMuonMtCut", m_min_mt);
    declareProperty("HighMuonMtCut", m_max_mt);

    declareProperty("Triggers", m_triggers);
    declareProperty("CutOnTrigger", m_cut_on_trigger);
    declareProperty("TriggerToolMuon", m_trigger_tool);
    declareProperty("TriggerMatchingTool", m_trig_match_tool);
}

MuonTauTPSelAlg::~MuonTauTPSelAlg() = default;

StatusCode MuonTauTPSelAlg::initialize() {
    if (!m_accept_oc && !m_accept_sc) {
        ATH_MSG_FATAL("If no same and oppsite charge events are accepted then where is the sense of running this alg?");
        return StatusCode::FAILURE;
    }
    if (m_muon_container.empty()) {
        ATH_MSG_FATAL("No muon container has been defined");
        return StatusCode::FAILURE;
    }
    if (m_tau_container.empty()) {
        ATH_MSG_FATAL("No tau container has been defined");
        return StatusCode::FAILURE;
    }
    if (m_min_mll > m_max_mll) {
        ATH_MSG_FATAL("Invalid configuration of the invariant mass window: " << m_min_mll << " - " << m_max_mll);
        return StatusCode::FAILURE;
    }
    if (m_min_mt > m_max_mt) {
        ATH_MSG_FATAL("Invalid configuration of the muon met transverse mass: " << m_min_mt << " - " << m_max_mt);
        return StatusCode::FAILURE;
    }
    ATH_CHECK(create_tree());
    /// Check the jet configuration
    ATH_CHECK(m_jet_calib_tool.retrieve());
    if (m_jet_calib_tool->GetBTagWPs().size() > 1) {
        ATH_MSG_FATAL("More than two b-tagging working points are configured. Only one is accepted");
        return StatusCode::FAILURE;
    } else if (m_jet_calib_tool->GetBTagWPs().empty()) {
        m_require_bjets = m_veto_bjets = false;
    } else {
        const std::string b_tag_wp = m_jet_calib_tool->GetBTagWPs()[0];
        m_bjet_accessor = std::make_unique<BoolAccessor>("isBtag_" + b_tag_wp);
    }

    ATH_CHECK(m_tree_tool.retrieve());
    ATH_CHECK(m_tree_tool->setup_tree(m_tree));
    /// Trigger initialization
    if (!m_triggers.empty()) {
        ATH_CHECK(m_trigger_tool.retrieve());
        ATH_CHECK(m_trig_match_tool.retrieve());
        for (const std::string& trig_item : m_triggers) {
            m_tree->addBranch(std::make_shared<TriggerDecisionBranch>(m_tree->common_tree(), m_trigger_tool, trig_item));
            if (!is_mc()) { m_tree->addBranch(std::make_shared<TriggerPreScaleBranch>(m_tree->common_tree(), m_trigger_tool, trig_item)); }
        }
    }
    ATH_CHECK(initialize_tree());

    m_cutflow_histo = new TH1I("CutFlow", "Dummy;;# events passed", CutFlowOrder::AllCuts, 1, CutFlowOrder::AllCuts + 1);
    ATH_CHECK(registerHisto(m_cutflow_histo));
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::AllEvents, "All");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::ElectronVeto, "e-veto");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::MuonSelection, "N_{#mu}==1");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::TauSelection, "N_{#tau}#geq1");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::PrimaryVtx, "Has Vtx.");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::Trigger, "Trigger");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::JetSelection, "Passes jets");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::MuonMtCut, "m_{T}(#mu,E_{T}^{miss})");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::AllMuonTracklets, "ID, ME & CB trk");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::ChargePair, "q_{#tau}#timesq_{#mu}");
    m_cutflow_histo->GetXaxis()->SetBinLabel(CutFlowOrder::MassCut, "m_{#tau,#mu}");
    return StatusCode::SUCCESS;
}

bool MuonTauTPSelAlg::pass_charge_sel(const int charge_prod) const {
    if (charge_prod < 0 && m_accept_oc)
        return true;
    else if (charge_prod > 0 && m_accept_sc)
        return true;
    return false;
}
bool MuonTauTPSelAlg::pass_trigger() {
    if (m_triggers.empty() || !m_cut_on_trigger) return true;
    for (const std::string& trigger : m_triggers) {
        if (m_trigger_tool->TrigDecision(trigger)) {
            ATH_MSG_DEBUG("Trigger " << trigger << " fired. The event is reasonable");
            m_cutflow_histo->Fill(CutFlowOrder::Trigger);
            return true;
        }
    }
    return false;
}
StatusCode MuonTauTPSelAlg::execute() {
    xAOD::TauJetContainer* taus = nullptr;
    xAOD::MuonContainer* muons = nullptr;
    const xAOD::ElectronContainer* elecs = nullptr;
    const xAOD::MissingETContainer* met_container = nullptr;

    ATH_CHECK(evtStore()->retrieve(taus, m_tau_container));
    ATH_CHECK(evtStore()->retrieve(muons, m_muon_container));
    ATH_CHECK(evtStore()->retrieve(met_container, m_met_container));

    m_cutflow_histo->Fill(CutFlowOrder::AllEvents);
    if (!m_elec_container.empty()) {
        ATH_CHECK(evtStore()->retrieve(elecs, m_elec_container));
        if (!elecs->empty()) {
            ATH_MSG_DEBUG("Found an additional electron in the event. Veto it " << elecs->size());
            return StatusCode::SUCCESS;
        }
        m_cutflow_histo->Fill(CutFlowOrder::ElectronVeto);
    }

    if (muons->size() != 1) {
        ATH_MSG_DEBUG("The muon container does not have the proper size " << muons->size());
        return StatusCode::SUCCESS;
    }
    m_cutflow_histo->Fill(CutFlowOrder::MuonSelection);

    if (taus->empty()) {
        ATH_MSG_DEBUG("Found either an empty tau container " << taus->size() << ". Terminate method");
        return StatusCode::SUCCESS;
    }
    m_cutflow_histo->Fill(CutFlowOrder::TauSelection);

    m_tree->release_common_containers();
    if (!m_tree->get_prim_vtx()) {
        ATH_MSG_DEBUG("No valid vertex has been found");
        return StatusCode::SUCCESS;
    }
    m_cutflow_histo->Fill(CutFlowOrder::PrimaryVtx);

    // next check the triggers
    if (!pass_trigger()) {
        ATH_MSG_DEBUG("No suitable trigger has been fired. Remove the event");
        return StatusCode::SUCCESS;
    }
    /// Check whether the b-jet selection is satified
    if (!pass_jet_selection()) {
        ATH_MSG_DEBUG("Event fails jet requirement. Remove it");
        return StatusCode::SUCCESS;
    }

    const xAOD::MissingET* met_obj = GetMET_obj("Final", met_container);
    if (!met_obj) {
        ATH_MSG_FATAL("No final has been found in MET container " << m_met_container);
        return StatusCode::FAILURE;
    }

    for (xAOD::Muon* mu : *muons) {
        /// Mt selection
        float mt = LeptonsInTaus::CalcMt(mu, met_obj);
        if (mt < m_min_mt || mt > m_max_mt) continue;
        m_cutflow_histo->Fill(CutFlowOrder::MuonMtCut);

        const xAOD::TrackParticle* id_track = mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
        const xAOD::TrackParticle* cb_track = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
        const xAOD::TrackParticle* ms_track = mu->trackParticle(xAOD::Muon::TrackParticleType::ExtrapolatedMuonSpectrometerTrackParticle);
        if (!id_track || !cb_track || !ms_track) continue;
        m_cutflow_histo->Fill(CutFlowOrder::AllMuonTracklets);
        for (xAOD::TauJet* tau : *taus) {
            /// Charge selection
            const int charge_prod = mu->charge() * tau->charge();
            if (!pass_charge_sel(charge_prod)) continue;
            m_cutflow_histo->Fill(CutFlowOrder::ChargePair);

            /// Invariant mass cut
            const TLorentzVector inv_p4 = tau->p4() + mu->p4();
            if (inv_p4.M() < m_min_mll || inv_p4.M() > m_max_mll) continue;
            m_cutflow_histo->Fill(CutFlowOrder::MassCut);
            ATH_CHECK(m_tree_tool->fill(tau, mu));
        }
    }
    return StatusCode::SUCCESS;
}

bool MuonTauTPSelAlg::pass_jet_selection() {
    if (m_jet_calib_tool->numberOfBadJets()) return false;
    unsigned int n_b_jets = 0;
    if (m_veto_bjets != m_require_bjets) {
        for (const xAOD::Jet* sel_jet : *m_jet_calib_tool->GetJets()) {
            ATH_MSG_DEBUG("Check jet " << sel_jet->pt() / 1.e3 << " GeV, eta:" << sel_jet->eta() << ", phi: " << sel_jet->phi()
                                       << " isB: " << (*m_bjet_accessor)(*sel_jet));
            n_b_jets += (*m_bjet_accessor)(*sel_jet);
        }
    }
    if (m_veto_bjets == m_require_bjets || m_veto_bjets == m_require_bjets || (m_veto_bjets && !n_b_jets) ||
        (m_require_bjets && n_b_jets)) {
        m_cutflow_histo->Fill(CutFlowOrder::JetSelection);
        return true;
    }
    return false;
}
