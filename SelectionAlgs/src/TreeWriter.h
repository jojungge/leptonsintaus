#ifndef TREEWRITERALG_H_
#define TREEWRITERALG_H_

#include <AthenaBaseComps/AthAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <TH1.h>

#include <memory>

/// Forward declarations
namespace LeptonsInTaus {
    class LeptonsInTauTree;
    class VertexBranch;
}  // namespace LeptonsInTaus

class ITHistSvc;
class IIFFTruthClassifier;
class IMuonJetCalibTool;
class TreeWriter : public AthAlgorithm {
public:
    /// Constructor with parameters:
    TreeWriter(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    virtual ~TreeWriter();

    /// Athena algorithm's Hooks
    StatusCode finalize() override final;

private:
    bool m_is_mc;
    bool m_write_prw;
    bool m_use_common_tree;

    /// Configured iff truth classifier
    ServiceHandle<ITHistSvc> m_histSvc;
    std::string m_file_stream;
    std::string m_tree_name;
    std::string m_tree_path;

protected:
    /// Tree instance
    std::shared_ptr<LeptonsInTaus::LeptonsInTauTree> m_tree;

    /// IFF truth classifier tool to determine the particles origin
    ToolHandle<IIFFTruthClassifier> m_iff_truth_classifier;
    /// Jet calibration tool to get more insight about the jet activity in the event
    ToolHandle<IMuonJetCalibTool> m_jet_calib_tool;

    bool is_mc() const;
    StatusCode create_tree();
    StatusCode initialize_tree();

    std::string file_stream() const;

    std::string tree_path() const;

    StatusCode registerHisto(TH1* histo);
};

#endif
