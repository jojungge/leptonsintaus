#include "METMakerAlg.h"

#include <METUtilities/METHelpers.h>
#include <MuonPerformanceCore/MetBranch.h>
#include <TreeTools/Defs.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMissingET/MissingETAssociationMap.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>
METMakerAlg::METMakerAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_elec_container(""),
    m_muon_container(""),
    m_tau_container(""),
    m_out_container(""),
    m_jet_calib_tool(),
    m_metMaker(),
    m_metSystTool(),
    m_met_key(""),
    m_met_map_key(""),
    m_EleRefTerm("RefEle"),
    m_MuoRefTerm("Muons"),
    m_TauRefTerm("RefTau"),
    m_JetRefTerm("RefJet"),
    m_PhoRefTerm("RefGamma"),
    m_TrackSoftTerm("PVSoftTrk"),
    m_CaloSoftTerm("SoftClus"),
    m_FinalMetTerm("Final"),
    m_FinalTrackTerm(""),
    m_trkJetsyst(false),
    m_trkMETsyst(false),
    m_isMC(false) {
    declareProperty("ElectronContainer", m_elec_container);
    declareProperty("MuonContainer", m_muon_container);
    declareProperty("TauContainer", m_tau_container);
    declareProperty("JetCalibrationTool", m_jet_calib_tool);

    declareProperty("OutContainer", m_out_container);

    declareProperty("MetContainer", m_met_key);
    declareProperty("MetAssociationMap", m_met_map_key);

    declareProperty("METMaker", m_metMaker, "The METMaker instance");
    declareProperty("METSystTool", m_metSystTool, "The METSystematicsTool instance");

    declareProperty("ElectronSoftTerm", m_EleRefTerm);
    declareProperty("MuonSoftTerm", m_MuoRefTerm);
    declareProperty("TauSoftTerm", m_TauRefTerm);
    declareProperty("JetSoftTerm", m_JetRefTerm);
    declareProperty("PhotonSoftTerm", m_PhoRefTerm);

    declareProperty("TrackSoftTerm", m_TrackSoftTerm);
    declareProperty("CaloSoftTerm", m_CaloSoftTerm);
    declareProperty("FinalMET", m_FinalMetTerm);
    declareProperty("FinalTrackMET", m_FinalTrackTerm);
    declareProperty("isMC", m_isMC);
}
StatusCode METMakerAlg::initialize() {
    if (m_out_container.empty()) {
        ATH_MSG_FATAL("The MET cannot be stored anywhere");
        return StatusCode::FAILURE;
    }
    ATH_CHECK(m_metMaker.retrieve());
    // ATH_CHECK(m_metSystTool.retrieve());
    ATH_CHECK(m_jet_calib_tool.retrieve());
    return StatusCode::SUCCESS;
}
StatusCode METMakerAlg::execute() {
    /// Electrons
    xAOD::ElectronContainer* elec_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(elec_container, m_elec_container));
    /// Muons
    xAOD::MuonContainer* muon_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(muon_container, m_muon_container));

    met::addGhostMuonsToJets(*muon_container, *m_jet_calib_tool->GetJets());

    /// Taus
    xAOD::TauJetContainer* tau_container = nullptr;
    if (!m_tau_container.empty()) { ATH_CHECK(evtStore()->retrieve(tau_container, m_tau_container)); }

    const xAOD::MissingETAssociationMap* MetMap = nullptr;
    ATH_CHECK(evtStore()->retrieve(MetMap, m_met_map_key));
    MetMap->resetObjSelectionFlags();

    /// Make the new MET container
    xAOD::MissingETContainer* met_container = nullptr;
    ATH_CHECK(CreateContainer(m_out_container, met_container));

    const xAOD::IParticleContainer* invisible_container = nullptr;
    bool doJvt = true;
    std::string softTerm = m_TrackSoftTerm;
    ATH_CHECK(markInvisible(met_container, invisible_container));
    ATH_CHECK(addContainerToMet(met_container, elec_container, xAOD::Type::ObjectType::Electron, invisible_container));
    ATH_CHECK(addContainerToMet(met_container, muon_container, xAOD::Type::ObjectType::Muon, invisible_container));
    ATH_CHECK(addContainerToMet(met_container, tau_container, xAOD::Type::ObjectType::Tau, invisible_container));
    // ATH_CHECK(addContainerToMet(met_container, photons, xAOD::Type::ObjectType::Photon, invisible_container));
    ATH_CHECK(buildMET(met_container, softTerm, doJvt, invisible_container, false));

    return StatusCode::SUCCESS;
}

StatusCode METMakerAlg::CreateContainer(const std::string& cont_name, xAOD::MissingETContainer*& Cont) {
    Cont = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* Aux = new xAOD::MissingETAuxContainer;
    Cont->setStore(Aux);
    const std::string store_name = cont_name.empty() ? m_out_container : cont_name;
    ATH_CHECK(evtStore()->record(Cont, store_name));
    ATH_CHECK(evtStore()->record(Aux, store_name + "Aux."));
    return StatusCode::SUCCESS;
}

std::string METMakerAlg::referenceTerm(xAOD::Type::ObjectType Type) const {
    if (Type == xAOD::Type::ObjectType::Electron)
        return m_EleRefTerm;
    else if (Type == xAOD::Type::ObjectType::Muon)
        return m_MuoRefTerm;
    else if (Type == xAOD::Type::ObjectType::Tau)
        return m_TauRefTerm;
    else if (Type == xAOD::Type::ObjectType::Jet)
        return m_JetRefTerm;
    else if (Type == xAOD::Type::ObjectType::Photon)
        return m_PhoRefTerm;
    ATH_MSG_ERROR("Invalid reference term asked");
    return std::string("");
}

StatusCode METMakerAlg::addContainerToMet(xAOD::MissingETContainer* MET, const xAOD::IParticleContainer* particles,
                                          xAOD::Type::ObjectType type, const xAOD::IParticleContainer* invisible) {
    const xAOD::MissingETAssociationMap* MetMap = nullptr;
    ATH_CHECK(evtStore()->retrieve(MetMap, m_met_map_key));
    std::string ref_term = referenceTerm(type);
    if (ref_term.empty()) return StatusCode::FAILURE;
    if (!particles) return StatusCode::SUCCESS;
    if (!invisible || invisible->empty()) { return m_metMaker->rebuildMET(ref_term, type, MET, particles, MetMap); }
    xAOD::IParticleContainer invis_free(SG::VIEW_ELEMENTS);
    for (xAOD::IParticle* met_part : *particles) {
        if (!LeptonsInTaus::isInContainer(invisible, met_part)) { invis_free.push_back(met_part); }
    }
    ATH_CHECK(m_metMaker->rebuildMET(ref_term, type, MET, &invis_free, MetMap));
    return StatusCode::SUCCESS;
}

StatusCode METMakerAlg::markInvisible(xAOD::MissingETContainer* MET, const xAOD::IParticleContainer* invisible) {
    const xAOD::MissingETAssociationMap* MetMap = nullptr;
    ATH_CHECK(evtStore()->retrieve(MetMap, m_met_map_key));
    if (invisible && !invisible->empty()) { ATH_CHECK(m_metMaker->markInvisible(invisible, MetMap, MET)); }
    return StatusCode::SUCCESS;
}

StatusCode METMakerAlg::buildMET(xAOD::MissingETContainer* MET, const std::string& softTerm, bool doJvt,
                                 const xAOD::IParticleContainer* invisible, bool build_track) {
    if (m_JetRefTerm.empty()) return StatusCode::FAILURE;
    //
    //  No invisible container is given -> pass everything directly to the
    //  MET interface tool

    const xAOD::MissingETAssociationMap* MetMap = nullptr;
    ATH_CHECK(evtStore()->retrieve(MetMap, m_met_map_key));

    const xAOD::MissingETContainer* raw_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(raw_container, m_met_key));

    if (!invisible || invisible->empty()) {
        if (!build_track)
            ATH_CHECK(m_metMaker->rebuildJetMET(m_JetRefTerm, softTerm, MET, m_jet_calib_tool->GetJets(), raw_container, MetMap, doJvt));
        else
            ATH_CHECK(m_metMaker->rebuildTrackMET(m_JetRefTerm, softTerm, MET, m_jet_calib_tool->GetJets(), raw_container, MetMap, doJvt));
    } else {
        xAOD::JetContainer invis_free(SG::VIEW_ELEMENTS);
        for (auto jet : *m_jet_calib_tool->GetJets()) {
            if (!LeptonsInTaus::isInContainer(invisible, jet)) invis_free.push_back(jet);
        }
        if (!build_track) {
            ATH_CHECK(m_metMaker->rebuildJetMET(m_JetRefTerm, softTerm, MET, &invis_free, raw_container, MetMap, doJvt));
        } else {
            ATH_CHECK(m_metMaker->rebuildTrackMET(m_JetRefTerm, softTerm, MET, &invis_free, raw_container, MetMap, doJvt));
        }
    }
    xAOD::MissingET* softTerm_MET = GetMET_obj(softTerm, MET);
    if (m_isMC && false) {
        if (!build_track && m_metSystTool->applyCorrection(*softTerm_MET) != CP::CorrectionCode::Ok) {
            ATH_MSG_WARNING("Something went wrong with the systematics");
        } else if (build_track) {
            if (m_trkMETsyst && m_metSystTool->applyCorrection(*softTerm_MET, MetMap) != CP::CorrectionCode::Ok) {
                ATH_MSG_WARNING("GetMET: Failed to apply MET track (PVSoftTrk) systematics.");
            }
            xAOD::MissingET* jetTerm_MET = GetMET_obj(m_JetRefTerm, MET);
            if (m_trkJetsyst && m_metSystTool->applyCorrection(*jetTerm_MET, MetMap) != CP::CorrectionCode::Ok) {
                ATH_MSG_WARNING("GetMET: Failed to apply MET track (RefJet) systematics.");
            }
        }
    }
    ATH_CHECK(m_metMaker->buildMETSum(build_track ? m_FinalTrackTerm : m_FinalMetTerm, MET, softTerm_MET->source()));

    return StatusCode::SUCCESS;
}
