#include "MuonSelectionAlg.h"

#include <IsolationSelection/IIsolationSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonTPInterfaces/IMuonTPTrigUtils.h>
#include <MuonTPTools/Utils.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTracking/VertexContainer.h>
MuonSelectionAlg::~MuonSelectionAlg() = default;
MuonSelectionAlg::MuonSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_sel_tool(""),
    m_iso_tool(""),
    m_matching_tool(""),
    m_triggers(),
    m_in_cointainer(""),
    m_out_container(""),
    m_pt_cut(1.e4),
    m_eta_cut(2.5),
    m_d0_sig_cut(-1.),
    m_z0_sinT_cut(-1.) {
    declareProperty("SelectionTool", m_sel_tool);
    declareProperty("IsoTool", m_iso_tool);
    declareProperty("TrigMatchingTool", m_matching_tool);
    declareProperty("Triggers", m_triggers);
    declareProperty("InContainer", m_in_cointainer);
    declareProperty("OutContainer", m_out_container);
    declareProperty("MinPt", m_pt_cut);
    declareProperty("MaxEta", m_eta_cut);
    declareProperty("D0Significance", m_d0_sig_cut);
    declareProperty("Z0SinTheta", m_z0_sinT_cut);
}
StatusCode MuonSelectionAlg::initialize() {
    if (m_in_cointainer.empty()) {
        ATH_MSG_FATAL("No input container has been given");
        return StatusCode::FAILURE;
    }
    if (m_out_container.empty()) {
        ATH_MSG_FATAL("Where to store the selected objects");
        return StatusCode::FAILURE;
    }
    if (m_triggers.size()) {
        ATH_CHECK(m_matching_tool.retrieve());
        ATH_MSG_INFO("Will only select muons matched to one of the listed triggers:");
        for (const auto& trig : m_triggers) { ATH_MSG_INFO("  *** " << trig); }
    }
    ATH_CHECK(m_sel_tool.retrieve());
    if (!m_iso_tool.empty()) ATH_CHECK(m_iso_tool.retrieve());
    return StatusCode::SUCCESS;
}
StatusCode MuonSelectionAlg::execute() {
    xAOD::MuonContainer* in_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(in_container, m_in_cointainer));
    xAOD::MuonContainer* out_container = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
    ATH_CHECK(evtStore()->record(out_container, m_out_container));
    const xAOD::Vertex* prim_vtx = nullptr;
    const xAOD::EventInfo* ev_info = nullptr;
    if (m_d0_sig_cut > 0 || m_z0_sinT_cut > 0) {
        const xAOD::VertexContainer* vtx_container = nullptr;
        ATH_CHECK(evtStore()->retrieve(vtx_container, "PrimaryVertices"));
        ATH_CHECK(evtStore()->retrieve(ev_info, "EventInfo"));
        for (const auto& vx : *vtx_container) {
            if (vx->vertexType() == xAOD::VxType::PriVtx) {
                prim_vtx = vx;
                break;
            }
        }
        if (!prim_vtx) {
            ATH_MSG_DEBUG("No primary vertex found. Skip event because IP cuts are enabled");
            return StatusCode::SUCCESS;
        }
    }

    for (xAOD::Muon* muon : *in_container) {
        ATH_MSG_DEBUG("Check muon " << muon->pt() / 1.e3 << " GeV, eta: " << muon->eta() << ", phi: " << muon->phi()
                                    << ", Q: " << muon->charge() << " quality: " << muon->quality());
        if (muon->pt() < m_pt_cut) {
            continue;
        } else if (m_eta_cut > 0 && std::abs(muon->eta()) > m_eta_cut) {
            continue;
        } else if (!m_sel_tool->accept(*muon)) {
            continue;
        } else if (!m_iso_tool.empty() && !m_iso_tool->accept(*muon)) {
            continue;
        }

        if (m_d0_sig_cut > 0.) {
            const xAOD::TrackParticle* trk = muon->primaryTrackParticle();
            float d0sign = 1.e3;
            try {
                d0sign = xAOD::TrackingHelpers::d0significance(trk, ev_info->beamPosSigmaX(), ev_info->beamPosSigmaY(),
                                                               ev_info->beamPosSigmaXY());
            } catch (...) { d0sign = 1.e3; }
            /// The muon failed the d0 significance cut
            if (std::abs(d0sign) > m_d0_sig_cut) continue;
        }
        if (m_z0_sinT_cut > 0.) {
            const xAOD::TrackParticle* trk = muon->primaryTrackParticle();
            float z0_sinT = (trk->z0() + trk->vz() - prim_vtx->z()) / std::cosh(muon->eta());
            if (std::abs(z0_sinT) > m_z0_sinT_cut) continue;
        }

        if (!is_matched(muon)) continue;
        out_container->push_back(muon);
    }
    return StatusCode::SUCCESS;
}
bool MuonSelectionAlg::is_matched(const xAOD::Muon* muon) const {
    if (m_triggers.empty()) return true;
    for (const auto& trig : m_triggers) {
        ATH_MSG_DEBUG("Test item " << trig);
        if (m_matching_tool->match(*muon, trig)) {
            ATH_MSG_DEBUG("Matching to trigger " << trig << " succeeded.");
            return true;
        }
    }
    ATH_MSG_DEBUG("Trigger matching failed");
    return false;
}
