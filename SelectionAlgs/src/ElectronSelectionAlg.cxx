#include "ElectronSelectionAlg.h"

#include <xAODEgamma/ElectronContainer.h>
namespace {
    constexpr float max_eta_accep = 2.47;
    constexpr float low_calo_eta = 1.37;
    constexpr float high_calo_eta = 1.52;
}  // namespace

ElectronSelectionAlg::ElectronSelectionAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_sel_tool(""),
    m_use_sel_decoration(false),
    m_decor_name("DFCommonElectronsLHMedium"),
    m_sel_acc(nullptr),
    m_in_cointainer(""),
    m_out_container(""),
    m_min_pt(5.e3),
    m_exclude_calo_gap(true) {
    declareProperty("SelectionTool", m_sel_tool);
    declareProperty("UseSelectionDecoration", m_use_sel_decoration);
    declareProperty("SelectionDecorator", m_decor_name);
    declareProperty("InContainer", m_in_cointainer);
    declareProperty("OutContainer", m_out_container);
    declareProperty("MinPt", m_min_pt);
    declareProperty("VetoCaloGap", m_exclude_calo_gap);
}
StatusCode ElectronSelectionAlg::initialize() {
    if (m_in_cointainer.empty()) {
        ATH_MSG_FATAL("No input container has been given");
        return StatusCode::FAILURE;
    }
    if (m_out_container.empty()) {
        ATH_MSG_FATAL("Where to store the selected objects");
        return StatusCode::FAILURE;
    }
    if (!m_use_sel_decoration) {
        ATH_CHECK(m_sel_tool.retrieve());
    } else {
        ATH_MSG_INFO("Use " << m_decor_name << " to decide whether the electron passed the corresponding working point");
        m_sel_acc = std::make_unique<CharAccessor>(m_decor_name);
    }
    return StatusCode::SUCCESS;
}
StatusCode ElectronSelectionAlg::execute() {
    xAOD::ElectronContainer* in_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(in_container, m_in_cointainer));
    xAOD::ElectronContainer* out_container = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    ATH_CHECK(evtStore()->record(out_container, m_out_container));
    for (xAOD::Electron* elec : *in_container) {
        if (elec->pt() < m_min_pt) {
            continue;
        } else if (!elec->caloCluster()) {
            continue;
        } else if (!elec->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)) {
            continue;
        }
        const float abs_eta = std::abs(elec->caloCluster()->etaBE(2));
        if (abs_eta > max_eta_accep) continue;
        if (m_exclude_calo_gap && abs_eta >= low_calo_eta && abs_eta <= high_calo_eta) { continue; }

        if ((!m_use_sel_decoration && m_sel_tool->accept(elec)) || (m_use_sel_decoration && (*m_sel_acc)(*elec)))
            out_container->push_back(elec);
    }
    return StatusCode::SUCCESS;
}
