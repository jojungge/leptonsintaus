#include "TreeWriter.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/Property.h>
#include <IFFTruthClassifier/IIFFTruthClassifier.h>
#include <MuonPerformanceCore/MuonEventInfoBranch.h>
#include <MuonTPTools/JetBranch.h>
#include <TreeTools/LeptonsInTausTree.h>
#include <TreeTools/TrackingBranches.h>
bool TreeWriter::is_mc() const { return m_is_mc; }
TreeWriter::TreeWriter(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_is_mc(false),
    m_write_prw(false),
    m_use_common_tree(true),
    m_histSvc("THistSvc", name),
    m_file_stream("LEPTONINTAU"),
    m_tree_name(""),
    m_tree_path(""),
    m_tree(nullptr),
    m_iff_truth_classifier(""),
    m_jet_calib_tool("") {
    declareProperty("FileStream", m_file_stream);
    declareProperty("TreePath", m_tree_path);
    declareProperty("TreeName", m_tree_name);
    declareProperty("isMC", m_is_mc);
    declareProperty("writePRW", m_write_prw);
    declareProperty("useCommonTree", m_use_common_tree);

    declareProperty("IFFTruthClassifier", m_iff_truth_classifier);
    declareProperty("JetCalibrationTool", m_jet_calib_tool);
}
TreeWriter::~TreeWriter() {}
StatusCode TreeWriter::finalize() {
    ATH_CHECK(m_tree->finalize());
    return StatusCode::SUCCESS;
}
StatusCode TreeWriter::create_tree() {
    ATH_CHECK(m_histSvc.retrieve());
    if (m_is_mc) ATH_CHECK(m_iff_truth_classifier.retrieve());
    if (m_tree_name.empty()) {
        ATH_MSG_FATAL("Empty tree names are not allowed");
        return StatusCode::FAILURE;
    }
    m_tree = std::make_shared<LeptonsInTaus::LeptonsInTauTree>(m_tree_name, m_tree_path);
    m_tree->set_file_stream(file_stream());
    if (m_use_common_tree && !MuonTreeCollector::getCollector()->get(file_stream())) {
        MuonTreeCollector::getCollector()->register_common(std::make_shared<MuonAnalysisTree>("CommonTree", ""), file_stream());
    }
    if (m_use_common_tree) {
        m_tree->add_friend(MuonTreeCollector::getCollector()->get(file_stream()));
    } else {
        m_tree->do_not_use_friends();
    }
    if (!m_tree->addBranch(std::make_shared<MuonEventInfoBranch>(m_tree, m_is_mc, m_write_prw))) {
        ATH_MSG_FATAL("Failed to initialize the basic event information");
        return StatusCode::FAILURE;
    }
    if (!m_jet_calib_tool.empty()) {
        ATH_CHECK(m_jet_calib_tool.retrieve());
        if (!m_tree->addBranch(std::make_shared<NumJetBranch>(*m_tree, m_jet_calib_tool->collectionFlag(), m_jet_calib_tool))) {
            return StatusCode::FAILURE;
        }
        if (!m_tree->addBranch(std::make_shared<LeadingJetBranch>(*m_tree, m_jet_calib_tool->collectionFlag(), m_jet_calib_tool))) {
            return StatusCode::FAILURE;
        }
    }
    /// Initialize the trees
    return StatusCode::SUCCESS;
}
StatusCode TreeWriter::initialize_tree() {
    ATH_CHECK(m_tree->init(evtStore(), m_histSvc));
    return StatusCode::SUCCESS;
}
std::string TreeWriter::file_stream() const { return m_file_stream; }
std::string TreeWriter::tree_path() const { return m_tree_path; }
StatusCode TreeWriter::registerHisto(TH1* histo) {
    if (!histo) {
        ATH_MSG_FATAL("No histogram pointer given");
        return StatusCode::SUCCESS;
    }
    ATH_CHECK(m_histSvc->regHist("/" + file_stream() + "/" + tree_path() + "/" + histo->GetName(), histo));
    return StatusCode::SUCCESS;
}
