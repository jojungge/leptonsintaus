#include "StonjekTaus.h"

#include <MuonTPTools/Utils.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>
StonjekTaus::StonjekTaus(const std::string& name, ISvcLocator* pSvcLocator) :
    ShallowCopyMakerAlgorithm(name, pSvcLocator),
    m_tau_container(""),
    m_muon_container(""),
    m_ev(0),
    m_mu_tau_pairs(0),
    m_di_mu_pairs(0),
    m_di_tau_pairs(0) {
    declareProperty("TauContainer", m_tau_container);
    declareProperty("MuonContainer", m_muon_container);
}
StatusCode StonjekTaus::initialize() { return StatusCode::SUCCESS; }
StatusCode StonjekTaus::finalize() {
    ATH_MSG_INFO("All events:     " << m_ev);
    ATH_MSG_INFO("Di-tau pairs:   " << m_di_tau_pairs << "= " << float(m_di_tau_pairs) / float(m_ev) * 100. << "%");
    ATH_MSG_INFO("Di-muon pairs:  " << m_di_mu_pairs << " = " << float(m_di_mu_pairs) / float(m_ev) * 100. << "%");
    ATH_MSG_INFO("Tau-muon pairs: " << m_mu_tau_pairs << " = " << float(m_mu_tau_pairs) / float(m_ev) * 100. << "%");
    return StatusCode::SUCCESS;
}
StatusCode StonjekTaus::execute() {
    const xAOD::TauJetContainer* tau_container = nullptr;
    const xAOD::MuonContainer* muon_container = nullptr;
    ATH_CHECK(evtStore()->retrieve(tau_container, m_tau_container));
    ATH_CHECK(evtStore()->retrieve(muon_container, m_muon_container));
    unsigned int n_mu(0), n_tau(0);
    for (const auto& mu : *muon_container) {
        const xAOD::TruthParticle* truth_mu =
            getTruthMatchedParticle(mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle));
        // PromptParticle(truth_mu);
        n_mu += (truth_mu && getParticleTruthOrigin(truth_mu) == 13);
    }
    for (const auto& tau : *tau_container) {
        const xAOD::TruthParticle* truth_tau = getTruthMatchedParticle(tau);
        n_tau += (truth_tau && getParticleTruthOrigin(truth_tau) == 13);
    }
    ++m_ev;
    m_di_tau_pairs += (n_tau == 2);
    m_di_mu_pairs += (n_mu == 2);
    m_mu_tau_pairs += (n_mu == 1) && (n_tau == 1);

    return StatusCode::SUCCESS;
}
