from AthenaCommon.AppMgr import ServiceMgr
from MuonPerformanceAlgs.CommonMuonTPConfig import AddPRWAlg, getTPAlgSequence, GetTrigUtils
from MuonPerformanceAlgs.CommonMuonTPConfig import isAF2, isOnDAOD, GetRightMuonContainer
from MuonPerformanceAlgs.IsolationBuilder import AddIsolationBuilderAlgs
import logging


def SetupIFFTool():
    tool_name = "IFFClassificationTool"
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, tool_name):
        my_tool = CfgMgr.IFFTruthClassifier(tool_name)
        ToolSvc += my_tool
    return getattr(ToolSvc, tool_name)


def SetupTauCalibrationTool(tool_name="TauCalibrationTool"):
    from AthenaCommon.AppMgr import ToolSvc
    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauSmearingTool
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)
    the_tool = TauAnalysisTools__TauSmearingTool(tool_name)
    ToolSvc += the_tool
    return the_tool


### Tau calibration alg
def AddTauCalibrationAlg(use_tp_alg=True):
    from AthenaCommon.AlgSequence import AlgSequence
    the_job = getTPAlgSequence() if use_tp_alg else AlgSequence()
    alg_name = "TauCalibrationAlg"
    if hasattr(the_job, alg_name): return getattr(the_job, alg_name)
    from SelectionAlgs.SelectionAlgsConf import CalibratedTauProvider
    the_job += CalibratedTauProvider(alg_name,
                                     CalibrationTool=SetupTauCalibrationTool(),
                                     InpuContainer="TauJets",
                                     OutputContainer="CalibratedTaus")
    return getattr(the_job, alg_name)


### Define the tau calibration alg
def SetupTauSelectionTool(WP="TauID_Tight"):
    tool_name = "TauSelectionTool_%s" % (WP)
    from AthenaCommon.AppMgr import ToolSvc
    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauSelectionTool
    from ClusterSubmission.Utils import ResolvePath
    #from AthenaCommon.Constants import VERBOSE
    config_file = ResolvePath("SelectionAlgs/%s.conf" % (WP))
    if not config_file:
        logging.error("Failed to setup the Tau SelectionTool for WP %s" % (WP))
        exit(1)
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)
    the_tool = TauAnalysisTools__TauSelectionTool(tool_name, ConfigPath=config_file)
    ToolSvc += the_tool
    return the_tool


def SetupTauEfficiencyTool(WP="Tau_Medium"):
    tool_name = "TauRecoEfficiencyTool_%s" % (WP)
    from AthenaCommon.AppMgr import ToolSvc
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)

    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauEfficiencyCorrectionsTool
    the_tool = TauAnalysisTools__TauEfficiencyCorrectionsTool(tool_name, TauSelectionTool=SetupTauSelectionTool(WP), isAFII=isAF2())
    if hasattr(ToolSvc, "MuonTPprwTool"):
        the_tool.PileupReweightingTool = ToolSvc.MuonTPprwTool
    ToolSvc += the_tool
    return the_tool


def AddTauSelectionAlg(WP="TauID_Medium", use_tp_alg=True, MuonOR_WP="Loose"):
    from AthenaCommon.AlgSequence import AlgSequence
    the_job = getTPAlgSequence() if use_tp_alg else AlgSequence()
    alg_name = "TauSelectionAlg_%s" % (WP)
    if hasattr(the_job, alg_name): return getattr(the_job, alg_name)
    from SelectionAlgs.SelectionAlgsConf import TauSelectionAlg
    from MuonPerformanceAlgs.CommonMuonTPConfig import isData, GetRightMuonContainer
    #muon_or_container = AddMuonSelectionAlg(WP=MuonOR_WP, use_tp_alg=use_tp_alg)
    #muon_or_container.Triggers = []
    the_alg = TauSelectionAlg(
        alg_name,
        InContainer=AddTauCalibrationAlg().OutputContainer,
        OutContainer="SelectedTaus_%s" % (WP),
        SelectionTool=SetupTauSelectionTool(WP),
        TrigMatchingTool=SetupTriggerMatchingTool(),
        TauMuonOR=0.2,
        #MuonContainerForOR=muon_or_container.OutContainer)
        MuonContainerForOR=GetRightMuonContainer())
    if not isData(): the_alg.RecoEffiTool = SetupTauEfficiencyTool(WP)
    the_job += the_alg
    return the_alg


### Define the electron calibration tool
def SetupElectronCalibrationTool(tool_name="ElectronCalibrationTool"):
    from AthenaCommon import CfgMgr
    from AthenaCommon.AppMgr import ToolSvc
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)
    Tool = CfgMgr.CP__EgammaCalibrationAndSmearingTool(tool_name,
                                                       ESModel="es2018_R21_v0",
                                                       decorrelationModel="1NP_v1",
                                                       useAFII=1 if isAF2() else 0)
    ToolSvc += Tool
    return Tool


def AddElectronCalibrationAlg(use_tp_alg=True):
    from AthenaCommon.AlgSequence import AlgSequence
    from AthenaCommon import CfgMgr
    the_job = getTPAlgSequence() if use_tp_alg else AlgSequence()
    alg_name = "ElectronCalibrationAlg"
    if hasattr(the_job, alg_name): return getattr(the_job, alg_name)
    from SelectionAlgs.SelectionAlgsConf import CalibratedTauProvider
    the_job += CfgMgr.CP__CalibratedEgammaProvider(alg_name,
                                                   Tool=SetupElectronCalibrationTool(),
                                                   Input="Electrons",
                                                   Output="CalibratedElectrons")

    return getattr(the_job, alg_name)


def AddElectronSelectionTool(WP="LHMedium"):
    tool_name = "ElectronSelectionTool_%s" % (WP)
    from AthenaCommon.AppMgr import ToolSvc
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)
    from ElectronPhotonSelectorTools.ElectronPhotonSelectorToolsConf import AsgElectronLikelihoodTool
    the_tool = AsgElectronLikelihoodTool(tool_name, WorkingPoint=WP)
    ToolSvc += the_tool
    return the_tool


def AddElectronSelectionAlg(WP="LHMedium", use_decorator=True, use_tp_alg=True):
    alg_name = "ElectronSelectionAlg_%s" % (WP)
    from AthenaCommon.AlgSequence import AlgSequence
    from SelectionAlgs.SelectionAlgsConf import ElectronSelectionAlg
    the_job = getTPAlgSequence() if use_tp_alg else AlgSequence()
    if hasattr(the_job, alg_name): return getattr(the_job, alg_name)
    the_alg = ElectronSelectionAlg(alg_name,
                                   SelectionTool=AddElectronSelectionTool(WP),
                                   UseSelectionDecoration=use_decorator,
                                   SelectionDecorator="DFCommonElectrons" + WP,
                                   InContainer=AddElectronCalibrationAlg().Output,
                                   OutContainer="SelectedElectrons" + WP)
    the_job += the_alg
    return getattr(the_job, alg_name)


def AddMuonSelectionAlg(WP="Medium", min_pt=5.e3, use_tp_alg=True, iso_wp=""):
    alg_name = "MuonSelectionAlg_%s%s_%.0f" % (WP, "_" + iso_wp if len(iso_wp) else iso_wp, min_pt)
    from AthenaCommon.AlgSequence import AlgSequence
    the_job = getTPAlgSequence() if use_tp_alg else AlgSequence()
    if hasattr(the_job, alg_name): return getattr(the_job, alg_name)
    from SelectionAlgs.SelectionAlgsConf import MuonSelectionAlg
    from MuonPerformanceAlgs.CommonMuonTPConfig import GetMuonSelectionTool, AddIsolationSelectionTool
    from MuonPerformanceAlgs.TriggerLists import GetTriggerList_Zmumu
    MuQuality = 1  #Use Medium as default
    if WP == "Loose": MuQuality = 2
    elif WP == "Medium": MuQuality = 1
    elif WP == "Tight": MuQuality = 0
    the_alg = MuonSelectionAlg(alg_name,
                               SelectionTool=GetMuonSelectionTool(MuQuality=MuQuality),
                               TrigMatchingTool=SetupTriggerMatchingTool(),
                               Triggers=GetTriggerList_Zmumu()['HLT'],
                               InContainer=GetRightMuonContainer(),
                               OutContainer="SelectedMuons_%s%s_%.0f" % (WP, "_" + iso_wp if len(iso_wp) else iso_wp, min_pt),
                               MinPt=min_pt)
    if len(iso_wp) > 0:
        the_alg.IsoTool = AddIsolationSelectionTool(iso_wp)
    the_job += the_alg
    return the_alg


def SetupMETMakerTool():
    tool_name = "METMakerTool"
    from AthenaCommon.AppMgr import ToolSvc
    if hasattr(ToolSvc, tool_name): return getattr(ToolSvc, tool_name)
    from AthenaCommon import CfgMgr
    the_tool = CfgMgr.met__METMaker(tool_name,
                                    DoPFlow=True,
                                    ORCaloTaggedMuons=True,
                                    DoSetMuonJetEMScale=True,
                                    DoRemoveMuonJets=True,
                                    UseGhostMuons=True,
                                    DoMuonEloss=True,
                                    GreedyPhotons=False,
                                    VeryGreedyPhotons=False,
                                    JetSelection="Tight")
    ToolSvc += the_tool
    return the_tool


def SetupTriggerMatchingTool():
    from AthenaCommon.AppMgr import ToolSvc
    tool_name = "TriggerMatchingTool"
    if not hasattr(ToolSvc, tool_name):
        from TriggerMatchingTool.TriggerMatchingToolConf import Trig__MatchingTool
        from MuonPerformanceAlgs.CommonMuonTPConfig import GetTrigDecisionTool
        trigmatch = Trig__MatchingTool(tool_name)
        trigmatch.TrigDecisionTool = GetTrigDecisionTool()
        ToolSvc += trigmatch
        return trigmatch
    return getattr(ToolSvc, tool_name)


def setupTreeTool(tool_name):
    from AthenaCommon.AppMgr import ToolSvc
    if not hasattr(ToolSvc, tool_name):
        from TreeTools.TreeToolsConf import LeptonsInTaus__DiMuonTTVATreeTool
        from MuonPerformanceAlgs.CommonMuonTPConfig import getFlags, GetMuonSelectionTool
        the_tool = LeptonsInTaus__DiMuonTTVATreeTool(
            tool_name,
            isMC=not getFlags().isData(),
            IFFTruthClassifier=SetupIFFTool(),
            MuonSelectionTool=GetMuonSelectionTool(MuQuality=2),
        )
        ToolSvc += the_tool

    return getattr(ToolSvc, tool_name)
