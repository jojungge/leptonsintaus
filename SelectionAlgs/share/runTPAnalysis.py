from AthenaCommon.AppMgr import ServiceMgr
from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from AthenaCommon.AppMgr import theApp
from AthenaServices.AthenaServicesConf import AthenaEventLoopMgr
from GaudiSvc.GaudiSvcConf import THistSvc
import AthenaPoolCnvSvc.ReadAthenaPool
from MuonPerformanceAlgs.JetCalibrationSetup import SetupJetCalibrations, AddJetCalibAlg, GetBtagEfficiencyTool, GetBtagSelectionTool
from MuonPerformanceAlgs.CommonMuonTPConfig import AddPRWAlg, AddMuonTPMetaAlg, AddCalibratedMuonsProvider, getTPAlgSequence
from MuonPerformanceAlgs.CommonMuonTPConfig import isAF2, isOnDAOD, GetRightMuonContainer, GetTrigUtils
from MuonPerformanceAlgs.CommonMuonTPConfig import isData as isData_Stonjek
from MuonPerformanceAlgs.IsolationBuilder import AddIsolationBuilderAlgs
from SelectionAlgs.ToolSetup import SetupIFFTool, AddTauSelectionAlg, AddElectronSelectionAlg, setupTreeTool
from SelectionAlgs.ToolSetup import SetupMETMakerTool, AddMuonSelectionAlg, SetupTriggerMatchingTool
from AthenaCommon.Constants import DEBUG
####

#--------------------------------------------------------------
# Reduce the event loop spam a bit
#--------------------------------------------------------------

ServiceMgr += AthenaEventLoopMgr(EventPrintoutInterval=1000)
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output += ["MUONTP DATAFILE='ZtaumuTagAndProbe.root' OPT='RECREATE'"]
# Finally, extend the space for the tool names in the log messages a bit
ServiceMgr.MessageSvc.Format = "% F%67W%S%7W%R%T %0W%M"

### Calibration and meta algs
AddPRWAlg()
AddMuonTPMetaAlg("MUONTP")
AddCalibratedMuonsProvider()

#### Now let's come to the actual TP algo for OC
muon_triggers = [
    "HLT_mu14",
    "HLT_mu18",
    "HLT_mu20",
    'HLT_mu20_iloose_L1MU15',
    'HLT_mu24_iloose',
    'HLT_mu24_iloose_L1MU15',
    'HLT_mu24_ivarloose',
    'HLT_mu24_ivarloose_L1MU15',
    'HLT_mu24_imedium',
    "HLT_mu24_ivarmedium",
    "HLT_mu26_imedium",
    "HLT_mu26_ivarmedium",
    "HLT_mu28_imedium",
    "HLT_mu28_ivarmedium",
    "HLT_mu50",
    "HLT_mu60",
]
tau_muon_trigers = [
    ### Electron tau triggers
    #'HLT_e17_lhmedium_iloose_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_iloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25',
    #'HLT_e17_lhmedium_iloose_tau25_medium1_tracktwo_L1EM15TAU12I-J25',
    #'HLT_e17_lhmedium_iloose_tau25_medium1_tracktwo_xe50',
    #'HLT_e17_lhmedium_iloose_tau25_perf_ptonly_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_ivarloose_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_ivarloose_tau25_medium1_tracktwo_L1EM15TAU12I-J25',
    #'HLT_e17_lhmedium_ivarloose_tau25_perf_ptonly_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo_L1EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_iloose_tau25_medium1_tracktwo_xe50',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_xe50',
    #'HLT_e17_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo_L1EM15HI_TAU20IM_2TAU15_J25_2J20_3J15',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_L1DR-EM15TAU12I',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_L1EM15TAU12I-J25',
    #'HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50',
    #'HLT_e17_lhmedium_nod0_tau80_medium1_tracktwo',
    #'HLT_e17_lhmedium_tau25_medium1_tracktwo',
    #'HLT_e17_lhmedium_tau25_medium1_tracktwo_L1DR-EM15TAU12I',
    #'HLT_e17_lhmedium_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25',
    #'HLT_e17_lhmedium_tau25_medium1_tracktwo_L1EM15HI_2TAU12IM',
    #'HLT_e17_lhmedium_tau25_medium1_tracktwo_xe50',
    #'HLT_e17_lhmedium_tau80_medium1_tracktwo',
    #'HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo',

    ### Photon - tau triggers
    #'HLT_g10_etcut_L1EM8I_mu10_iloose_taumass',
    #'HLT_g10_etcut_L1EM8I_mu10_ivarloose_taumass',
    #'HLT_g10_etcut_L1EM8I_mu10_taumass',
    #'HLT_g10_etcut_mu10_iloose_taumass_L1LFV-EM8I',
    #'HLT_g10_etcut_mu10_ivarloose_taumass_L1LFV-EM8I',
    #'HLT_g10_etcut_mu10_taumass',
    #'HLT_g10_loose_L1EM8I_mu10_iloose_taumass',
    #'HLT_g10_loose_L1EM8I_mu10_ivarloose_taumass',
    #'HLT_g10_loose_mu10_iloose_taumass_L1LFV-EM8I',
    #'HLT_g10_loose_mu10_ivarloose_taumass_L1LFV-EM8I',
    #'HLT_g20_etcut_L1EM15_mu4_iloose_taumass',
    #'HLT_g20_etcut_L1EM15_mu4_ivarloose_taumass',
    #'HLT_g20_etcut_L1EM15_mu4_taumass',
    #'HLT_g20_etcut_mu4_iloose_taumass_L1LFV-EM15I',
    #'HLT_g20_etcut_mu4_ivarloose_taumass_L1LFV-EM15I',
    #'HLT_g20_etcut_mu4_taumass',
    #'HLT_g20_loose_L1EM15_mu4_iloose_taumass',
    #'HLT_g20_loose_L1EM15_mu4_ivarloose_taumass',
    #'HLT_g20_loose_mu4_iloose_taumass_L1LFV-EM15I',
    #'HLT_g20_loose_mu4_ivarloose_taumass_L1LFV-EM15I',
    #'HLT_g25_medium_L1EM24VHI_tau25_singlepion_tracktwo_50mVis10000',
    #'HLT_g25_medium_tau25_singlepion_tracktwo_50mVis10000',
    #'HLT_g25_medium_tau25_singlepion_tracktwo_50mVis10000_L130M-EM20ITAU12',
    #'HLT_g35_medium_tau25_perf_tracktwo_L1TAU12',

    ### Muon tau triggers
    'HLT_mu14_iloose_tau25_medium1_tracktwo',
    'HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR-MU10TAU12I',
    'HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR-MU10TAU12I_TAU12I-J25',
    'HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12I-J25',
    'HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM',
    'HLT_mu14_iloose_tau25_medium1_tracktwo_xe50',
    'HLT_mu14_iloose_tau25_perf_ptonly_L1MU10_TAU12IM',
    'HLT_mu14_iloose_tau25_perf_tracktwo',
    'HLT_mu14_iloose_tau35_medium1_tracktwo',
    'HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR-MU10TAU12I',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR-MU10TAU12I_TAU12I-J25',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12I-J25',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM',
    'HLT_mu14_ivarloose_tau25_medium1_tracktwo_xe50',
    'HLT_mu14_ivarloose_tau25_perf_ptonly_L1MU10_TAU12IM',
    'HLT_mu14_ivarloose_tau25_perf_tracktwo',
    'HLT_mu14_ivarloose_tau35_medium1_tracktwo',
    'HLT_mu14_ivarloose_tau35_medium1_tracktwo_L1MU10_TAU20IM_J25_2J20',
    'HLT_mu14_tau25_medium1_tracktwo',
    'HLT_mu14_tau25_medium1_tracktwo_L1DR-MU10TAU12I',
    'HLT_mu14_tau25_medium1_tracktwo_L1DR-MU10TAU12I_TAU12I-J25',
    'HLT_mu14_tau25_medium1_tracktwo_L1MU10_TAU12I-J25',
    'HLT_mu14_tau25_medium1_tracktwo_L1MU10_TAU12IM',
    'HLT_mu14_tau25_medium1_tracktwo_xe50',
    'HLT_mu14_tau35_medium1_tracktwo',
    ### Tau triggers
    'HLT_tau125_medium1_tracktwo',
    'HLT_tau125_medium1_tracktwo_tau50_medium1_tracktwo_L1TAU12',
    'HLT_tau125_perf_tracktwo',
    'HLT_tau160_idperf_tracktwo',
    'HLT_tau160_medium1HighptH_tracktwo',
    'HLT_tau160_medium1HighptL_tracktwo',
    'HLT_tau160_medium1HighptM_tracktwo',
    'HLT_tau160_medium1_tracktwo',
    'HLT_tau160_medium1_tracktwo_L1TAU100',
    'HLT_tau160_perf_tracktwo',
    'HLT_tau200_medium1_tracktwo_L1TAU100',
    'HLT_tau25_dikaon_tracktwo',
    'HLT_tau25_dikaonmass_tracktwo',
    'HLT_tau25_dikaonmasstight_tracktwo',
    'HLT_tau25_dikaontight_tracktwo',
    'HLT_tau25_dipion1_tracktwo',
    'HLT_tau25_dipion2_tracktwo',
    'HLT_tau25_idperf_tracktwo',
    'HLT_tau25_kaonpi1_tracktwo',
    'HLT_tau25_kaonpi2_tracktwo',
    'HLT_tau25_loose1_tracktwo',
    'HLT_tau25_medium1_tracktwo',
    'HLT_tau25_medium1_tracktwo_L1TAU12',
    'HLT_tau25_perf_tracktwo',
    'HLT_tau25_singlepion_tracktwo',
    'HLT_tau25_singlepiontight_tracktwo',
    'HLT_tau25_tight1_tracktwo',
    'HLT_tau35_dikaon_tracktwo_L1TAU12',
    'HLT_tau35_dikaonmass_tracktwo_L1TAU12',
    'HLT_tau35_dikaonmasstight_tracktwo_L1TAU12',
    'HLT_tau35_dikaontight_tracktwo_L1TAU12',
    'HLT_tau35_dipion1loose_tracktwo_L1TAU12',
    'HLT_tau35_dipion2_tracktwo_L1TAU12',
    'HLT_tau35_kaonpi1_tracktwo_L1TAU12',
    'HLT_tau35_kaonpi2_tracktwo_L1TAU12',
    'HLT_tau35_loose1_tracktwo',
    'HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo',
    'HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo_L1DR-TAU20ITAU12I',
    'HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo_L1TAU20IM_2TAU12IM',
    'HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo_L1TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo',
    'HLT_tau35_medium1_tracktwo_L1TAU20',
    'HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12',
    'HLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR27_L1DR25-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR27_L1DR25-TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1BOX-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_notautsf_L1DR-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_notautsf_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_tautsf_L1DR-TAU20ITAU12I',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_tautsf_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50',
    'HLT_tau35_medium1_tracktwo_xe70_L1XE45',
    'HLT_tau35_perf_tracktwo',
    'HLT_tau35_perf_tracktwo_L1TAU20_tau25_perf_tracktwo_L1TAU12',
    'HLT_tau35_perf_tracktwo_tau25_perf_tracktwo',
    'HLT_tau35_perf_tracktwo_tau25_perf_tracktwo_ditauL',
    'HLT_tau35_perf_tracktwo_tau25_perf_tracktwo_ditauM',
    'HLT_tau35_perf_tracktwo_tau25_perf_tracktwo_ditauT',
    'HLT_tau35_tight1_tracktwo',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_03dR27_L1DR25-TAU20ITAU12I',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_03dR27_L1DR25-TAU20ITAU12I-J25',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_03dR30',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_03dR30_L1DR-TAU20ITAU12I',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_03dR30_L1DR-TAU20ITAU12I-J25',
    'HLT_tau35_tight1_tracktwo_tau25_tight1_tracktwo_L1TAU20IM_2TAU12IM',
    'HLT_tau50_medium1_tracktwo_L1TAU12',
    'HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50',
    'HLT_tau60_medium1_tracktwo_tau35_medium1_tracktwo',
    'HLT_tau80_medium1_tracktwo',
    'HLT_tau80_medium1_tracktwo_L1TAU60',
    'HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12',
    'HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12'
]
ele_sel_alg = AddElectronSelectionAlg()
tau_sel_alg = AddTauSelectionAlg("TauID_Loose")
tau_sel_alg.ElecContainerForOR = ele_sel_alg.OutContainer
#tau_sel_alg.OutputLevel = DEBUG
#ServiceMgr.MessageSvc.verboseLimit = 100000000
#ServiceMgr.MessageSvc.debugLimit = 100000000

#tau_sel_alg.TauElecOR = 0.2
muo_sel_alg = AddMuonSelectionAlg()
#muo_sel_alg.OutputLevel = DEBUG
muo_sel_alg.Triggers = []
tau_sel_alg.Triggers = []

SetupJetCalibrations("PFlow", useTPSeq=True)

## Set the proper b-tagging files to the calibration tool
jet_calib_tool = ToolSvc.MCPJetCalibAntiKt4EMPflow

cdi_file = "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root"
wp = "FixedCutBEff_85"
tag_alg = "DL1"
timestamp = "201903"
jet_calib_tool.BTagSFTools = [
    GetBtagEfficiencyTool("%s_BTagging%s" % (jet_calib_tool.JetCollection, timestamp), wp, tag_alg, cdi_file, "Envelope")
]
jet_calib_tool.BTaggingTools = [GetBtagSelectionTool("%s_BTagging%s" % (jet_calib_tool.JetCollection, timestamp), wp, tag_alg, cdi_file)]
jet_calib_tool.EtaCut = 2.8
the_job = getTPAlgSequence()
the_job.MuonFilter.Triggers = muon_triggers
the_job.MuonFilter.CutOnTrigger = False

#### Append the MET maker algorithm
from SelectionAlgs.SelectionAlgsConf import METMakerAlg
the_job += METMakerAlg("METMakerAlg",
                       ElectronContainer=ele_sel_alg.OutContainer,
                       MuonContainer=muo_sel_alg.OutContainer,
                       TauContainer=tau_sel_alg.OutContainer,
                       JetCalibrationTool=jet_calib_tool,
                       isMC=not isData_Stonjek(),
                       OutContainer="CalibratedPFlowMET",
                       METMaker=SetupMETMakerTool(),
                       MetContainer="MET_Core_AntiKt4EMPFlow",
                       MetAssociationMap="METAssoc_AntiKt4EMPFlow")
#
##
###
####     Run the tau muon selection - OC
###
##
#
tau_tree_tool_oc = setupTreeTool("TauMuonTPTreeTool_OC")
tau_tree_tool_oc.JetCalibrationTool = jet_calib_tool
tau_tree_tool_oc.MetContainer = "CalibratedPFlowMET"

from SelectionAlgs.SelectionAlgsConf import MuonTauTPSelAlg

the_job += MuonTauTPSelAlg(
    "TauMuonTPSelAlg_OC",
    ElectronContainer=ele_sel_alg.OutContainer,
    JetCalibrationTool=jet_calib_tool,
    MuonContainer=muo_sel_alg.OutContainer,
    TauContainer=tau_sel_alg.OutContainer,
    TreeTool=tau_tree_tool_oc,
    METContainer="CalibratedPFlowMET",
    AcceptOC=True,
    VetoBJets=False,
    RequireBJets=False,
    #CutOnTrigger=isData_Stonjek(),
    LowMassCut=40.e3,
    HighMassCut=90.e3,
    Triggers=muon_triggers + tau_muon_trigers,
    TriggerToolMuon=GetTrigUtils(),
    FileStream="MUONTP",
    TreePath="ZmumuTPMerged/Trees/MuonTauTagAndProbe_OC/",
    TreeName="TPTree_MuonTauTagAndProbe_OC",
    isMC=not isData_Stonjek(),
    IFFTruthClassifier=SetupIFFTool(),
    TriggerMatchingTool=SetupTriggerMatchingTool())
#
##
###
####     Same sign tau muon selection
###
##
#
tau_tree_tool_sc = setupTreeTool("TauMuonTPTreeTool_SC")
tau_tree_tool_sc.JetCalibrationTool = jet_calib_tool
tau_tree_tool_sc.MetContainer = "CalibratedPFlowMET"
the_job += MuonTauTPSelAlg("TauMuonTPSelAlg_SC",
                           ElectronContainer=ele_sel_alg.OutContainer,
                           JetCalibrationTool=jet_calib_tool,
                           MuonContainer=muo_sel_alg.OutContainer,
                           TauContainer=tau_sel_alg.OutContainer,
                           TreeTool=tau_tree_tool_sc,
                           METContainer="CalibratedPFlowMET",
                           AcceptOC=False,
                           AcceptSC=True,
                           VetoBJets=False,
                           RequireBJets=False,
                           LowMassCut=40.e3,
                           HighMassCut=90.e3,
                           Triggers=muon_triggers + tau_muon_trigers,
                           TriggerToolMuon=GetTrigUtils(),
                           FileStream="MUONTP",
                           TreePath="ZmumuTPMerged/Trees/MuonTauTagAndProbe_SC/",
                           TreeName="TPTree_MuonTauTagAndProbe_SC",
                           isMC=not isData_Stonjek(),
                           IFFTruthClassifier=SetupIFFTool(),
                           TriggerMatchingTool=SetupTriggerMatchingTool())

#
##
###
####     Run the di-muon selection - OC
###
##
#

tag_muon_sel_alg = AddMuonSelectionAlg(WP="Medium", min_pt=27.e3, use_tp_alg=True, iso_wp="FCTight")
tag_muon_sel_alg.Triggers = []
tag_muon_sel_alg.D0Significance = 3.
tag_muon_sel_alg.Z0SinTheta = 0.5
tag_muon_sel_alg.MaxEta = 2.7

from SelectionAlgs.SelectionAlgsConf import DiMuonTPSelAlg

muon_tree_tool_oc = setupTreeTool("DiMuonTPTreeTool_OC")
muon_tree_tool_oc.JetCalibrationTool = jet_calib_tool
muon_tree_tool_oc.MetContainer = "CalibratedPFlowMET"
muon_tree_tool_oc.isTauMuonTP = False
the_job += DiMuonTPSelAlg("DiMuonTPSelAlg_OC",
                          JetCalibrationTool=jet_calib_tool,
                          ProbeContainer=muo_sel_alg.OutContainer,
                          TagContainer=tag_muon_sel_alg.OutContainer,
                          TreeTool=muon_tree_tool_oc,
                          AcceptOC=True,
                          AcceptSC=False,
                          VetoBJets=False,
                          RequireBJets=False,
                          Triggers=muon_triggers,
                          TriggerToolMuon=GetTrigUtils(),
                          FileStream="MUONTP",
                          TreePath="ZmumuTPMerged/Trees/DiMuonTagAndProbe_OC/",
                          TreeName="TPTree_DiMuonTagAndProbe_OC",
                          isMC=not isData_Stonjek(),
                          IFFTruthClassifier=SetupIFFTool(),
                          TriggerMatchingTool=SetupTriggerMatchingTool())
#
##
###
####     Run the di-muon selection - SC
###
##
#
muon_tree_tool_sc = setupTreeTool("DiMuonTPTreeTool_SC")
muon_tree_tool_sc.JetCalibrationTool = jet_calib_tool
muon_tree_tool_sc.MetContainer = "CalibratedPFlowMET"
muon_tree_tool_sc.isTauMuonTP = False

the_job += DiMuonTPSelAlg("DiMuonTPSelAlg_SC",
                          JetCalibrationTool=jet_calib_tool,
                          ProbeContainer=muo_sel_alg.OutContainer,
                          TagContainer=tag_muon_sel_alg.OutContainer,
                          TreeTool=muon_tree_tool_sc,
                          AcceptOC=False,
                          AcceptSC=True,
                          VetoBJets=False,
                          RequireBJets=False,
                          Triggers=muon_triggers,
                          TriggerToolMuon=GetTrigUtils(),
                          FileStream="MUONTP",
                          TreePath="ZmumuTPMerged/Trees/DiMuonTagAndProbe_SC/",
                          TreeName="TPTree_DiMuonTagAndProbe_SC",
                          isMC=not isData_Stonjek(),
                          IFFTruthClassifier=SetupIFFTool(),
                          TriggerMatchingTool=SetupTriggerMatchingTool())
