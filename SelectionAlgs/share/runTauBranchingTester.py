from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from AthenaCommon.AppMgr import theApp
from AthenaServices.AthenaServicesConf import AthenaEventLoopMgr
from GaudiSvc.GaudiSvcConf import THistSvc
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AppMgr import ServiceMgr
from AthenaCommon.AlgSequence import AlgSequence
from AthenaServices.AthenaServicesConf import AthenaEventLoopMgr
from MuonPerformanceAlgs.CommonMuonTPConfig import AddCalibratedMuonsProvider, getTPAlgSequence
from SelectionAlgs.ToolSetup import AddTauSelectionAlg, AddMuonSelectionAlg, AddTauCalibrationAlg, setupTreeTool
from AthenaCommon.Constants import DEBUG
####

#--------------------------------------------------------------
# Reduce the event loop spam a bit
#--------------------------------------------------------------

ServiceMgr += AthenaEventLoopMgr(EventPrintoutInterval=1000)
# Finally, extend the space for the tool names in the log messages a bit
ServiceMgr.MessageSvc.Format = "% F%67W%S%7W%R%T %0W%M"

### Calibration and meta algs
AddCalibratedMuonsProvider()
AddTauCalibrationAlg(False)
the_job = AlgSequence()

tau_sel_alg = AddTauSelectionAlg(WP="TauID_Tight", use_tp_alg=False)
tau_sel_alg.OutContainer = "StonjekTightTau"
from SelectionAlgs.SelectionAlgsConf import StonjekTaus
the_job += StonjekTaus("StonjekStonjek", TauContainer="CalibratedTaus", MuonContainer="CalibratedMuons")

the_job += StonjekTaus("StonjekStonjekTight", TauContainer="StonjekTightTau", MuonContainer="CalibratedMuons")

muon_sel_alg = AddMuonSelectionAlg(use_tp_alg=False)
muon_sel_alg.Triggers = []
the_job += StonjekTaus("StonjekMediumTight", TauContainer="StonjekTightTau", MuonContainer=muon_sel_alg.OutContainer)

the_job = getTPAlgSequence()
the_job.MuonFilter.CutOnTrigger = False
muon_sel_alg = AddMuonSelectionAlg(WP="MediumStonjek", use_tp_alg=True)
muon_sel_alg.Triggers = []
muon_sel_alg.OutContainer = "StonjekMediumMuon"
the_job += StonjekTaus("StonjekStonjekMediumTight", TauContainer="StonjekTightTau", MuonContainer=muon_sel_alg.OutContainer)
