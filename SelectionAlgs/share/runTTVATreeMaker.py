from AthenaCommon.AppMgr import ServiceMgr
from AthenaCommon.AlgSequence import AlgSequence
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from AthenaCommon.AppMgr import theApp
from AthenaServices.AthenaServicesConf import AthenaEventLoopMgr
from GaudiSvc.GaudiSvcConf import THistSvc
import AthenaPoolCnvSvc.ReadAthenaPool
from MuonPerformanceAlgs.JetCalibrationSetup import SetupJetCalibrations, AddJetCalibAlg
from MuonPerformanceAlgs.CommonMuonTPConfig import AddPRWAlg, AddMuonTPMetaAlg, AddCalibratedMuonsProvider, getTPAlgSequence
from MuonPerformanceAlgs.CommonMuonTPConfig import isAF2, isOnDAOD, GetRightMuonContainer
from MuonPerformanceAlgs.CommonMuonTPConfig import isData as isData_Stonjek
from MuonPerformanceAlgs.CommonMuonTPConfig import GetMuonSelectionTool, GetTrigUtils
from MuonPerformanceAlgs.CommonMuonTPConfig import AddPromptLeptonDecorators
from MuonPerformanceAlgs.IsolationBuilder import AddIsolationBuilderAlgs
from SelectionAlgs.ToolSetup import SetupIFFTool, AddMuonSelectionAlg

####

#--------------------------------------------------------------
# Reduce the event loop spam a bit
#--------------------------------------------------------------

ServiceMgr += AthenaEventLoopMgr(EventPrintoutInterval=1000)
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output += ["LEPTONINTAU DATAFILE='lepton_in_tau.root' OPT='RECREATE'"]
# Finally, extend the space for the tool names in the log messages a bit
ServiceMgr.MessageSvc.Format = "% F%67W%S%7W%R%T %0W%M"

AddPRWAlg()
AddMuonTPMetaAlg("LEPTONINTAU")
AddCalibratedMuonsProvider()

#if len(ServiceMgr.EventSelector.InputCollections) > 0 and not isOnDAOD():
#    include("MuonPerformanceAlgs/RecExCommon_for_TP.py")
#    AddPromptLeptonDecorators()

SetupJetCalibrations("PFlow", useTPSeq=True)
### Standard algs to run
AddIsolationBuilderAlgs(run_muons=True, run_indet=False, run_metracks=False)
top_seq = AlgSequence()
the_job = getTPAlgSequence()
the_job.MuonFilter.CutOnTrigger = False

from SelectionAlgs.SelectionAlgsConf import MuonTreeWriter
muo_sel_alg = AddMuonSelectionAlg(WP="Medium", min_pt=5.e3, use_tp_alg=True)
the_alg = CfgMgr.MuonTreeWriter("MuonTreeAlg",
                                MuonContainer=muo_sel_alg.OutputContainer,
                                MuonSelectionTool=GetMuonSelectionTool(),
                                IFFTruthClassifier=SetupIFFTool(),
                                TreeName="MuonTree",
                                JetCalibrationTool=AddJetCalibAlg(useTPSeq=True).JetCalibTools[0],
                                useCommonTree=False,
                                doPromptLepton=False,
                                isMC=not isData_Stonjek(),
                                writePRW=hasattr(top_seq, "prwProvider"))

the_job += the_alg
